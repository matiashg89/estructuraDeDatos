-- 1. Pizzas
-- Tenemos los siguientes tipos de datos:
data Pizza = Prepizza | Capa Ingrediente Pizza deriving Show

data Ingrediente = Salsa | Queso | Jamon | Aceitunas Int deriving Show

pizza1 = (Capa Salsa 
			(Capa (Aceitunas 14)
				(Capa Jamon
					(Capa (Aceitunas 8) Prepizza))))

pizza2 = (Capa Salsa 
			(Capa Queso
				(Capa Queso
					(Capa Queso Prepizza))))

-- Definir las siguientes funciones:
-- Dada una pizza devuelve la lista de capas de ingredientes
cantidadDeCapas :: Pizza -> Int
cantidadDeCapas Prepizza = 0
cantidadDeCapas (Capa ing pizza) = 1 + (cantidadDeCapas pizza)

-- Dada una lista de ingredientes construye una pizza
armarPizza :: [Ingrediente] -> Pizza
armarPizza [] = Prepizza
armarPizza (x:xs) = Capa x (armarPizza xs)

--Le saca los ingredientes que sean jamón a la pizza
sacarJamon :: Pizza -> Pizza
sacarJamon Prepizza = Prepizza
sacarJamon (Capa ingrediente pizza) = if esMismoIngrediente ingrediente Jamon
										then (sacarJamon pizza)
										else (Capa ingrediente (sacarJamon pizza))

esMismoIngrediente :: Ingrediente -> Ingrediente -> Bool
esMismoIngrediente Salsa Salsa = True
esMismoIngrediente Queso Queso = True
esMismoIngrediente Jamon Jamon = True
esMismoIngrediente (Aceitunas x) (Aceitunas y) = True
esMismoIngrediente _ _ = False

-- Dice si una pizza tiene salsa y queso
tieneSoloSalsaYQueso :: Pizza -> Bool
tieneSoloSalsaYQueso Prepizza = True
tieneSoloSalsaYQueso (Capa ing pizza) = (esSalsaOQueso ing) && (tieneSoloSalsaYQueso pizza)
	
esSalsaOQueso :: Ingrediente -> Bool
esSalsaOQueso Salsa = True
esSalsaOQueso Queso = True
esSalsaOQueso _ = False

-- Recorre cada ingrediente y si es aceitunas duplica su cantidad
duplicarAceitunas :: Pizza -> Pizza
duplicarAceitunas Prepizza = Prepizza
duplicarAceitunas (Capa ing pizza) = Capa (duplicarSiEsAceituna ing) (duplicarAceitunas pizza)

duplicarSiEsAceituna :: Ingrediente -> Ingrediente
duplicarSiEsAceituna (Aceitunas cant) = (Aceitunas (cant*2))
duplicarSiEsAceituna x = x

-- Dada una lista de pizzas devuelve un par donde la primera componente es la cantidad de
-- ingredientes de la pizza, y la respectiva pizza como segunda componente.
cantCapasPorPizza :: [Pizza] -> [(Int, Pizza)]
cantCapasPorPizza [] = []
cantCapasPorPizza (x:xs) = [(cantidadDeCapas x,x)] ++ (cantCapasPorPizza xs)


-- 2. Mapa de tesoros (con bifurcaciones)
-- Un mapa de tesoros es un árbol con bifurcaciones que terminan en cofres. Cada bifurcación y
-- cada cofre tiene un objeto, que puede ser chatarra o un tesoro.
data Dir = Izq | Der deriving Show
data Objeto = Tesoro | Chatarra deriving Show
data Cofre = Cofre [Objeto] deriving Show
data Mapa = Fin Cofre | Bifurcacion Cofre Mapa Mapa deriving Show

mapa1 :: Mapa
mapa1 = (Bifurcacion (Cofre []) 
	        (Bifurcacion (Cofre []) (Fin (Cofre [Tesoro])) (Fin (Cofre [Chatarra])))
            (Fin (Cofre [Chatarra, Tesoro]))) 

mapa2 :: Mapa
mapa2 = (Bifurcacion (Cofre []) 
	        (Bifurcacion (Cofre []) (Fin (Cofre [])) (Bifurcacion (Cofre []) (Fin (Cofre [])) (Fin (Cofre []))))
            (Fin (Cofre [])))

mapa3:: Mapa
mapa3 =( Bifurcacion (Cofre []) 
			(
			  Bifurcacion (Cofre []) (Fin (Cofre []) ) (Fin (Cofre []) )
			) 
			(
				Bifurcacion (Cofre []) 
					(Bifurcacion (Cofre [Tesoro]) 
						(Fin (Cofre [])) 
						(Bifurcacion (Cofre [])
							(Fin (Cofre [])) 
							(Fin (Cofre []))
						)
					) 
					(Fin (Cofre []))
			)
		)

-- 1. Indica si hay un tesoro en alguna parte del mapa.
hayTesoro :: Mapa -> Bool
hayTesoro (Fin cofre) = (hayTesoroEnElCofre cofre)
hayTesoro (Bifurcacion cofre mapa1 mapa2) = (hayTesoroEnElCofre cofre) || (hayTesoro mapa1) || (hayTesoro mapa2)

hayTesoroEnElCofre :: Cofre -> Bool
hayTesoroEnElCofre (Cofre objetos) = (hayTesoroEnLaLista objetos)

hayTesoroEnLaLista :: [Objeto] -> Bool
hayTesoroEnLaLista [] = False
hayTesoroEnLaLista (x:xs) = (esMismoObjeto Tesoro x) || (hayTesoroEnLaLista xs)

esMismoObjeto :: Objeto -> Objeto -> Bool
esMismoObjeto Tesoro Tesoro = True
esMismoObjeto Chatarra Chatarra = True
esMismoObjeto _ _ = False

-- 2. Indica si al final del camino hay un tesoro. Nota: el final de un camino se representa con una
-- lista vacía de direcciones.
hayTesoroEn :: [Dir] -> Mapa -> Bool
hayTesoroEn _ (Fin cofre) = (hayTesoroEnElCofre cofre)
hayTesoroEn [] (Bifurcacion cofre mapa1 mapa2) = (hayTesoroEnElCofre cofre)
hayTesoroEn (x:xs) (Bifurcacion cofre mapa1 mapa2) = if esMismaDireccion x Izq
													 	then hayTesoroEn xs mapa1
													 	else hayTesoroEn xs mapa2
			
esMismaDireccion :: Dir -> Dir -> Bool
esMismaDireccion Der Der = True
esMismaDireccion Izq Izq = True
esMismaDireccion _ _ = False
											

-- 3. Indica el camino al tesoro. Precondición: existe un tesoro y es único.
caminoAlTesoro :: Mapa -> [Dir]
caminoAlTesoro (Fin c) = []
caminoAlTesoro (Bifurcacion cofre mapa1 mapa2) = if hayTesoroEnElCofre cofre 
													then [] 
													else if hayTesoro mapa1
														then [Izq] ++ (caminoAlTesoro mapa1)
														else [Der] ++ (caminoAlTesoro mapa2)

-- 4. Indica el camino de la rama más larga.
caminoDeLaRamaMasLarga :: Mapa -> [Dir]
caminoDeLaRamaMasLarga (Fin c) = []
caminoDeLaRamaMasLarga (Bifurcacion cofre mapa1 mapa2) = if tamRamaMasLarga mapa1 > tamRamaMasLarga mapa2
															then [Izq] ++ (caminoDeLaRamaMasLarga mapa1)
															else [Der] ++ (caminoDeLaRamaMasLarga mapa2)


tamRamaMasLarga :: Mapa -> Int
tamRamaMasLarga (Fin c) = 0
tamRamaMasLarga (Bifurcacion cofre mapa1 mapa2) = 1 + ( max (tamRamaMasLarga mapa1) (tamRamaMasLarga mapa2))

-- 5. Devuelve los tesoros separados por nivel en el árbol.
-- VER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- tesorosPorNivel :: Mapa -> [[Objeto]]
-- tesorosPorNivel (Fin c) = [[c]]
-- tesorosPorNivel (Bifurcacion cofre mapa1 mapa2) = mapTesoros(
-- 												  [cofre] : 
-- 												  (unirPorNivel (tesorosPorNivel mapa1) (tesorosPorNivel mapa2)))


-- unirPorNivel :: [[a]] -> [[a]] -> [[a]]
-- unirPorNivel [] x = x
-- unirPorNivel x [] = x
-- unirPorNivel (x:xs)(y:ys) = (x ++ y) : (unirPorNivel xs ys)


-- mapTesoros :: [[Cofre]] -> [[Objeto]]
-- mapTesoros [] = []
-- mapTesoros (xs:xss) = (separarCofresConTesoros xs) ++ (mapTesoros xss)

-- separarCofresConTesoros :: [Cofre] -> [[Objeto]]
-- separarCofresConTesoros [] = []
-- separarCofresConTesoros (x:xs) = (separarTesoros x) : (separarCofresConTesoros xs)

-- separarTesoros :: Cofre ->  [Objeto]
-- separarTesoros (Cofre []) = []
-- separarTesoros (Cofre objs) = (tesorosDelCofre  objs)

-- tesorosDelCofre :: [Objeto] -> [Objeto]
-- tesorosDelCofre [] = []
-- tesorosDelCofre (x:xs) = if esMismoObjeto x Tesoro
-- 							then [Tesoro] ++ (tesorosDelCofre xs)
-- 							else (tesorosDelCofre xs)

-- [
-- 	[Cofre []],
-- 	[Cofre [],Cofre []],
-- 	[Cofre [],Cofre [],Cofre [Tesoro],Cofre []],
-- 	[Cofre [],Cofre []],
-- 	[Cofre [],Cofre []]
-- ]

-- 6. Devuelve todos lo caminos en el mapa.
todosLosCaminos :: Mapa -> [[Dir]]
todosLosCaminos (Fin c) = [[]]
todosLosCaminos (Bifurcacion cofre mapa1 mapa2) = (agregarA Izq (todosLosCaminos mapa1))
												  ++
												  (agregarA Der (todosLosCaminos mapa2))

agregarA :: a -> [[a]] -> [[a]]
agregarA n []  = []
agregarA n (x:xs) =
	(n : x) : agregarA n xs

-- 3. Nave Espacial
data Componente = LanzaTorpedos | Motor Int | Almacen [Barril] deriving Show
data Barril = Comida | Oxigeno | Torpedo | Combustible deriving Show
data Sector = S SectorId [Componente] [Tripulante] deriving Show
type SectorId = String
type Tripulante = String
data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving Show
data Nave = N (Tree Sector) deriving Show

arbolSectores :: (Tree Sector)
arbolSectores = (NodeT (S "1" [LanzaTorpedos,(Motor 14), (Almacen [Comida,Oxigeno])] []) 
					(NodeT (S "2" [LanzaTorpedos] ["Juan"]) 
						(NodeT (S "44" [LanzaTorpedos,(Motor 16),(Almacen [Comida,Oxigeno])] []) EmptyT EmptyT) 
						(NodeT (S "5" [LanzaTorpedos] ["Matias"]) EmptyT EmptyT)
					)
					(NodeT (S "3" [LanzaTorpedos,(Almacen [Combustible])] []) 
						(NodeT (S "6" [LanzaTorpedos,(Motor 17)] []) EmptyT EmptyT) 
						(NodeT (S "47" [LanzaTorpedos] ["Matias"]) EmptyT EmptyT)
					)
				)

miNave :: Nave
miNave = (N arbolSectores)
-- 1. Propósito: Devuelve todos los sectores de la nave.
sectores :: Nave -> [SectorId]
sectores (N sec) = obtenerIdDeCadaSectorDelAarbol sec

obtenerIdDeCadaSectorDelAarbol :: (Tree Sector) -> [SectorId]
obtenerIdDeCadaSectorDelAarbol EmptyT = []
obtenerIdDeCadaSectorDelAarbol (NodeT sec ti td) = [(sectorID sec)] ++ (obtenerIdDeCadaSectorDelAarbol ti) ++ (obtenerIdDeCadaSectorDelAarbol td)

sectorID :: Sector -> SectorId
sectorID (S id _ _) = id

-- 2. Propósito: Devuelve la suma de poder de propulsión de todos los motores de la nave. Nota:
-- el poder de propulsión es el número que acompaña al constructor de motores.
poderDePropulsion :: Nave -> Int
poderDePropulsion (N sec) = obtenerMotoresYsumarPropulsion sec

obtenerMotoresYsumarPropulsion :: (Tree Sector) -> Int
obtenerMotoresYsumarPropulsion EmptyT = 0
obtenerMotoresYsumarPropulsion (NodeT sec ti td) = (propulsionDelSector sec) + (obtenerMotoresYsumarPropulsion ti) + (obtenerMotoresYsumarPropulsion td)

propulsionDelSector :: Sector -> Int
propulsionDelSector (S _ componentes _) = sumarPropulsionMotores componentes

sumarPropulsionMotores :: [Componente] -> Int
sumarPropulsionMotores [] = 0
sumarPropulsionMotores (x:xs) = if esMotor x
									then (propulsionMotor x) + (sumarPropulsionMotores xs)
									else (sumarPropulsionMotores xs)

esMotor :: Componente -> Bool
esMotor (Motor a) = True
esMotor _ = False

propulsionMotor :: Componente -> Int
propulsionMotor (Motor p) = p
propulsionMotor _ = 0

-- 3. Propósito: Devuelve todos los barriles de la nave.
barriles :: Nave -> [Barril]
barriles (N sec) = obtenerBarrillesDeLosSectores sec

obtenerBarrillesDeLosSectores :: (Tree Sector) -> [Barril]
obtenerBarrillesDeLosSectores EmptyT = []
obtenerBarrillesDeLosSectores (NodeT sec ti td) = 	(barrillesDelSector sec) ++
													(obtenerBarrillesDeLosSectores ti) ++
													(obtenerBarrillesDeLosSectores td)

barrillesDelSector :: Sector -> [Barril]
barrillesDelSector (S _ componentes _) = (barrilesDelComponente componentes)

barrilesDelComponente :: [Componente] -> [Barril]
barrilesDelComponente [] = []
barrilesDelComponente (x:xs) = if esAlmacen x
								then (barrilesDelAlmacen x) ++ (barrilesDelComponente xs) 
								else (barrilesDelComponente xs) 

esAlmacen :: Componente -> Bool
esAlmacen (Almacen x) = True
esAlmacen _ = False

barrilesDelAlmacen :: Componente -> [Barril]
barrilesDelAlmacen (Almacen x) = x

-- 4. Propósito: Añade una lista de componentes a un sector de la nave.
-- Nota: ese sector puede no existir, en cuyo caso no añade componentes.
agregarASector :: [Componente] -> SectorId -> Nave -> Nave
-- agregarASector [] _ nave = nave
agregarASector componentes idsector (N sec) = N (buscarSectorYAgregarComponentes componentes idsector sec)

buscarSectorYAgregarComponentes :: [Componente] -> SectorId -> (Tree Sector) -> (Tree Sector)
buscarSectorYAgregarComponentes cs id EmptyT = EmptyT
buscarSectorYAgregarComponentes comp sectorid  (NodeT sec ti td) = if (sectorid == idSector sec)
																		then NodeT (S sectorid (comp ++ (componentesDelSector sec)) (tripulantesDelSector sec)) ti td
																		else NodeT sec (buscarSectorYAgregarComponentes comp sectorid ti ) (buscarSectorYAgregarComponentes comp sectorid td )

idSector :: Sector -> SectorId
idSector (S id _ _ ) = id

componentesDelSector :: Sector -> [Componente]
componentesDelSector (S _ comp _) = comp

tripulantesDelSector :: Sector -> [Tripulante]
tripulantesDelSector (S _ _ trip) = trip 

-- 5. Propósito: Incorpora un tripulante a una lista de sectores de la nave.
-- Precondición: Todos los id de la lista existen en la nave.
asignarTripulanteA :: Tripulante -> [SectorId] -> Nave -> Nave
asignarTripulanteA _ [] nave = nave
asignarTripulanteA trip sectores (N sec) = N (buscarSectoresYAgregarTripulantes trip sectores sec)

buscarSectoresYAgregarTripulantes :: Tripulante -> [SectorId] -> (Tree Sector) -> (Tree Sector)
buscarSectoresYAgregarTripulantes _ _ EmptyT = EmptyT
buscarSectoresYAgregarTripulantes trip sectores (NodeT sec ti td) = if (existeSectorEnLista sectores (idSector sec))
																		then NodeT (S (idSector sec) (componentesDelSector sec) (trip : (tripulantesDelSector sec)) ) (buscarSectoresYAgregarTripulantes trip sectores ti) (buscarSectoresYAgregarTripulantes trip sectores td)
																		else NodeT sec (buscarSectoresYAgregarTripulantes trip sectores ti) (buscarSectoresYAgregarTripulantes trip sectores td)

existeSectorEnLista :: [SectorId] -> SectorId -> Bool
existeSectorEnLista [] _ = False
existeSectorEnLista (x:xs) sector = (x == sector) || (existeSectorEnLista xs sector)

-- 6. Propósito: Devuelve los sectores en donde aparece un tripulante dado.
sectoresAsignados :: Tripulante -> Nave -> [SectorId]
sectoresAsignados trip (N sec) = sectoresDelTripulante trip sec

sectoresDelTripulante :: Tripulante -> (Tree Sector) -> [SectorId]
sectoresDelTripulante _ EmptyT = []
sectoresDelTripulante trip (NodeT sec ti td) = if elTripulanteEsDelSector trip sec
												then (idSector sec) : (sectoresDelTripulante trip ti) ++ (sectoresDelTripulante trip td)
												else (sectoresDelTripulante trip ti) ++ (sectoresDelTripulante trip td)

elTripulanteEsDelSector :: Tripulante -> Sector -> Bool
elTripulanteEsDelSector trip (S _ _ tripulantes) = existeElTripulanteEnLaLista trip tripulantes

existeElTripulanteEnLaLista :: Tripulante -> [Tripulante] -> Bool
existeElTripulanteEnLaLista _ [] = False
existeElTripulanteEnLaLista trip (x:xs) = (x == trip) || (existeElTripulanteEnLaLista trip xs)

-- 7. Propósito: Devuelve la lista de tripulantes, sin elementos repetidos.
tripulantes :: Nave -> [Tripulante]
tripulantes (N sec) = sinRepetidos (tripulantesDeLosSectores sec)

tripulantesDeLosSectores :: (Tree Sector) -> [Tripulante]
tripulantesDeLosSectores EmptyT = []
tripulantesDeLosSectores (NodeT sec ti td) =  (tripulantesDelSector sec) ++ (tripulantesDeLosSectores ti) ++ (tripulantesDeLosSectores td)


sinRepetidos :: [Tripulante] -> [Tripulante]
sinRepetidos [] = []
sinRepetidos (x:xs) = if exiteTripulanteEn x xs
						then sinRepetidos xs
						else x : sinRepetidos xs

exiteTripulanteEn :: Tripulante -> [Tripulante] -> Bool
exiteTripulanteEn e [] = False
exiteTripulanteEn e (x:xs) =  (x == e) || (exiteTripulanteEn e xs)

-- 4. Manada de lobos
-- Los cazadores poseen nombre, una lista de especies de presas cazadas y 3 lobos a cargo.
-- Los exploradores poseen nombre, una lista de nombres de territorio explorado (nombres de bosques, ríos, etc.), y poseen 2 lobos a cargo.
-- Las crías poseen sólo un nombre y no poseen lobos a cargo.
-- La estructura es la siguiente:
type Presa = String -- nombre de presa
type Territorio = String -- nombre de territorio
type Nombre = String -- nombre de lobo
data Lobo = Cazador Nombre [Presa] Lobo Lobo Lobo | Explorador Nombre [Territorio] Lobo Lobo | Cria Nombre  deriving Show
data Manada = M Lobo  deriving Show

miManada :: Manada
miManada = M (Cazador "LoboCazador" ["","","","","",""]
				(Explorador "LoboExplorador_1" ["A"] 
					(Cria "Cria_1") 
					(Cria "Cria_2"))
				(Explorador "LoboExplorador_2" ["b"] 
					(Cria "Cria_3")
					(Cazador "LoboCazador_2" ["","","","","","","","","","",""]
						(Cria "")
						(Cria "")
						(Cria "")))
				(Cria "Cria_5"))

-- 2.Propósito: dada una manada, indica si la cantidad de alimento cazado es mayor a la cantidad de crías.
buenaCaza :: Manada -> Bool
buenaCaza m = cantidadAlimentoManada m > cantidadCriasManada m

cantidadAlimentoManada :: Manada -> Int
cantidadAlimentoManada (M lobo) = cantidadAlimentoLobo lobo

cantidadAlimentoLobo :: Lobo -> Int
cantidadAlimentoLobo (Cria _) = 0
cantidadAlimentoLobo (Explorador _ _ l1 l2) = (cantidadAlimentoLobo l1) + (cantidadAlimentoLobo l2)
cantidadAlimentoLobo (Cazador _ presas l1 l2 l3) =  (length presas) +
													(cantidadAlimentoLobo l1) + 
													(cantidadAlimentoLobo l2) + 
													(cantidadAlimentoLobo l3)


cantidadCriasManada :: Manada -> Int
cantidadCriasManada (M lobo) = cantidadCrias lobo

cantidadCrias :: Lobo -> Int
cantidadCrias (Cria _) = 1
cantidadCrias (Explorador _ _ l1 l2) = (cantidadCrias l1) + (cantidadCrias l2)
cantidadCrias (Cazador _ presas l1 l2 l3) = (cantidadCrias l1) + (cantidadCrias l2) + (cantidadCrias l3)

-- 3. Propósito: dada una manada, devuelve el nombre del lobo con más presas cazadas, junto
-- con su cantidad de presas. Nota: se considera que los exploradores y crías tienen cero presas
-- cazadas, y que podrían formar parte del resultado si es que no existen cazadores con más de
-- cero presas.
elAlfa :: Manada -> (Nombre, Int)
elAlfa (M lobo) = elMasCazador (elLoboAlfa lobo)

elLoboAlfa :: Lobo -> [(Nombre, Int)]
elLoboAlfa (Cria nombre) = []
elLoboAlfa (Explorador nombre _ l1 l2) = (elLoboAlfa l1) ++ (elLoboAlfa l2)	
elLoboAlfa (Cazador nombre presas l1 l2 l3) = 	(nombre, (length presas)) :
												(elLoboAlfa l1) ++
										 		(elLoboAlfa l2) ++
										 		(elLoboAlfa l3) 

elMasCazador :: [(Nombre, Int)] -> (Nombre, Int)
elMasCazador [] = ("",0)
elMasCazador (x:xs) = if snd x > snd (elMasCazador xs)
						then x
						else (elMasCazador xs)

-- 4. Propósito: dado un territorio y una manada, devuelve los nombres de los exploradores que
-- pasaron por dicho territorio.
losQueExploraron :: Territorio -> Manada -> [Nombre]
losQueExploraron ter (M lobo) = losQueExploraron' ter lobo

losQueExploraron' :: Territorio -> Lobo -> [Nombre]
losQueExploraron' ter (Cria _) = []
losQueExploraron' ter (Cazador _ _ l1 l2 l3) = (losQueExploraron' ter l1) ++
											   (losQueExploraron' ter l2) ++
											   (losQueExploraron' ter l3)		 
losQueExploraron' ter (Explorador nombre territorios l1 l2) = if exploroElTerritorio ter territorios 
															 	then [nombre] ++ (losQueExploraron' ter l1) ++ (losQueExploraron' ter l2)
															 	else (losQueExploraron' ter l1) ++ (losQueExploraron' ter l2)


exploroElTerritorio :: Territorio -> [Territorio] -> Bool
exploroElTerritorio territorio [] = False
exploroElTerritorio territorio (x:xs) = (territorio == x) || (exploroElTerritorio territorio xs)


-- 5. Propósito: dada una manada, denota la lista de los pares cuyo primer elemento es un territorio y cuyo segundo elemento es 
-- la lista de los nombres de los exploradores que exploraron dicho territorio. Los territorios no deben repetirse.
--VER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
exploradoresPorTerritorio :: Manada -> [(Territorio, [Nombre])]
exploradoresPorTerritorio (M lobo) = exploradoresPorTerritorio' lobo (todosLosTerritorios lobo)

todosLosTerritorios :: Lobo -> [Territorio]
todosLosTerritorios _ = ["A","b"]

exploradoresPorTerritorio' :: Lobo -> [Territorio] -> [(Territorio, [Nombre])]
exploradoresPorTerritorio' (Cria _) _ = []
exploradoresPorTerritorio' (Cazador _ _ l1 l2 l3 ) territorios = (exploradoresPorTerritorio' l1 territorios)++(exploradoresPorTerritorio' l2 territorios)++(exploradoresPorTerritorio' l3 territorios)
exploradoresPorTerritorio' (Explorador nombre territorios l1 l2) (x:xs) = if exploroElTerritorio x territorios
																			then [(x,[nombre])] ++ (exploradoresPorTerritorio' l1 xs)++(exploradoresPorTerritorio' l2 xs)
																			else (exploradoresPorTerritorio' l1 xs)++(exploradoresPorTerritorio' l2 xs)

-- 6.Propósito: dado un nombre de cazador y una manada, indica el nombre de todos los
-- cazadores que tienen como subordinado al cazador dado (directa o indirectamente).
-- Precondición: hay un cazador con dicho nombre y es único.
-- superioresDelCazador :: Nombre -> Manada -> [Nombre]	
-- superioresDelCazador n (M lobo) = superioresDelCazador' n lobo





superioresDelCazador :: Nombre -> Manada -> [Nombre]
superioresDelCazador nom (M l) = superioresDelCazadorL nom l

superioresDelCazadorL :: Nombre -> Lobo -> [Nombre]
superioresDelCazadorL nom (Cria _) = []
superioresDelCazadorL nom (Explorador _ _ l1 l2) = superioresDelCazadorL nom l1 ++ superioresDelCazadorL nom l2
superioresDelCazadorL nom (Cazador n xs l1 l2 l3) = 
    if esSubordinado nom (Cazador n xs l1 l2 l3)
        then n : superioresDelCazadorL nom l1 ++ superioresDelCazadorL nom l2 ++ superioresDelCazadorL nom l3
        else superioresDelCazadorL nom l1 ++ superioresDelCazadorL nom l2 ++ superioresDelCazadorL nom l3

esSubordinado :: Nombre -> Lobo -> Bool
esSubordinado nom (Cria _) = False
esSubordinado nom (Explorador _ _ l1 l2) = False
esSubordinado nom (Cazador n xs l1 l2 l3) = esSubordinadoDirecto nom (Cazador n xs l1 l2 l3) || esSubordinado nom l1 || esSubordinado nom l2 || esSubordinado nom l3

esSubordinadoDirecto :: Nombre -> Lobo -> Bool
esSubordinadoDirecto nom (Cria _) = False
esSubordinadoDirecto nom (Explorador _ _ l1 l2) = False
esSubordinadoDirecto nom (Cazador n _ l1 l2 l3) = nom == nombreLobo l1 || nom == nombreLobo l2 || nom == nombreLobo l3



