module BST (
	BST
	)
where 

data BST a = EmptyBST | NodeBST a (BST a) (BST a)



emptyBST :: BST a
emptyBST = EmptyBST

isEmptyBST :: Bst a -> Bool
isEmptyBST EmptyBST = True
isEmptyBST _ = False

-- Propósito: dado un BST dice si el elemento pertenece o no al árbol.
-- Costo: O(log N)
belongsBST :: Ord a => a -> Tree a -> Bool
belongsBST n (EmptyBST) = False
belongsBST n (NodeT a ti td) = if n == a
								then True
								else if n > a
										then belongsBST n td
										else belongsBST n tilookUpBST :: Ord a => BST a -> Maybe a

InsertBST :: Ord a => a -> BST a -> BST a
InsertBST n EmptyBST = NodeBST n EmptyBST EmptyBST
InsertBST n (NodeBST a ti td) = 
							if a == n
							then (NodeBST a ti td)
							else if n < a
								then NodeBST n (InsertBST n ti) td
								else NodeBST n ti (InsertBST n td)

deleteBST :: Ord a => a -> BST a -> BST a
deleteBST n EmptyBST = EmptyBST
deleteBST n (NodeBST a ti td) = if a == n 
									then -- lo borro
									else if a > n
										then deleteBST n 



-- toListBST :: BST a -> [a]
-- minBST :: BST a -> a
-- deleteMinBST :: BST a -> BST a

