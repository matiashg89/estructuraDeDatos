module Organizador(
	Organizador,
	nuevo,
	agregarPrograma,
	todosLosProgramas,
	autoresDe,
	programasDe,
	programaronJuntas,
	nroProgramasDePersona
) where

data Organizador = MKO (Map Checksum (Set Persona)) (Map Persona (Set Checksum))
data Persona = String
data Checksum = String

-- Invariantes de representacion
-- Todas las personas del set del primer map, tienen que estan includas como clave en el segundo map
-- Todos los checksum del sel del segundo map, tienen que estar incluidos como clave en el primer map
-- La relacion Checksum - Persona tiene que estar en ambos maps
-- En el primer map no puede haber personas repetidas por set
-- En el segundo map no puede haber checksum repetidos por set

-- Propósito: Un organizador vacío.
-- Eficiencia: O(1)
nuevo :: Organizador
nuevo = MKO emptyM emptyM

-- Propósito: Agrega al organizador un programa con el Checksum indicado; el conjunto es el conjunto de personas autores
-- de dicho programa.
-- Precondición: el identificador del programa que se agrega no fue usado previamente en el organizador, y el Set de personas
-- no está vacío.
-- Eficiencia: no hay ninguna garantía de eficiencia.
agregarPrograma :: Organizador -> Checksum -> Set Persona -> Organizador
agregarPrograma (MKO mapCheck mapPersonas) check setPersonas = MKO (agregarClaveAlMap mapCheck check setPersonas) (agregarCheckALasPersonas check (set2list setPersonas) mapPersonas)

agregarClaveAlMap :: Map Checksum (Set Persona) -> Checksum -> Set Persona -> Map Checksum (Set Persona)
agregarClaveAlMap mapCheck check setPersonas = assocM check setPersonas mapCheck

agregarCheckALasPersonas :: Checksum -> [Persona] -> Map Persona (Set Checksum) -> Map Persona (Set Checksum)
agregarCheckALasPersonas check (p:ps) mapPersonas = case lookupM p mapPersonas of
	 													Nothing -> error "Persona no encontrada"
	 													Just set -> assocM check (addS check set) (agregarCheckALasPersonas check ps mapPersonas)

-- Propósito: denota una lista con todos y cada uno de los códigos identificadores de programas del organizador.
-- Eficiencia: O(C) en peor caso, donde C es la cantidad de códigos en el organizador.
todosLosProgramas :: Organizador -> [Checksum]
todosLosProgramas (MKO mapCheck _ ) = domM mapCheck

-- Propósito: denota el conjunto de autores que aparecen en un programa determinado.
-- Precondición: el Checksum debe corresponder a un programa del organizador.
-- Eficiencia: O(log C) en peor caso, donde C es la cantidad total de programas del organizador.
autoresDe :: Organizador -> Checksum -> Set Persona
autoresDe (MKO mapCheck _ ) c = case lookupM c mapCheck of
								 Nothing -> error "Checksum no encontrado"
								 Just set -> set

-- Propósito: denota el conjunto de programas en los que participó una determinada persona.
-- Precondición: la persona debe existir en el organizador.
-- Eficiencia: O(log P) en peor caso, donde P es la cantidad total de personas del organizador.
programasDe :: Organizador -> Persona -> Set Checksum
programasDe (MKO _ mapPersonas ) p = case lookupM p mapPersonas of
								 		Nothing -> error "Persona no encontrada"
								 		Just set -> set

-- Propósito: dado un organizador y dos personas, denota verdadero si ambas son autores de algún software en común.
-- Precondición: las personas deben ser distintas.
-- Eficiencia: O(log P + C log C) en peor caso, donde P es la cantidad de personas distintas que aparecen en todos los
-- programas del organizador, y C la cantidad total de programas.
programaronJuntas :: Organizador -> Persona -> Persona -> Bool
programaronJuntas o p1 p2 = not isEmptyS (intersection (programasDe p1) (programasDe p2))

-- Propósito: dado un organizador y una persona, denota la cantidad de programas distintos en los que aparece.
-- Eficiencia: O(log P) en peor caso, donde P es la cantidad de personas del organizador.
nroProgramasDePersona :: Organizador -> Persona -> Int
nroProgramasDePersona (MKO _ mapPersonas ) p = case lookupM p mapPersonas of 
												 Nothing -> error "Persona no encontrada"
												 Just set = sizeS set