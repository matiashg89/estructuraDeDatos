import Organizador

-- a) Propósito: dadas dos personas y un organizador, denota el conjunto de aquellos programas en las que las personas
-- programaron juntas.
programasEnComun :: Persona -> Persona -> Organizador -> Set Checksum
programasEnComun p1 p2 o = intersection (programasDe o p1) (programasDe o p2)

-- b) Propósito: denota verdadero si la persona indicada aparece como autor de todos los programas del organizador.
esUnGranHacker :: Organizador -> Persona -> Bool
esUnGranHacker o p = (length (todosLosProgramas o)) == (nroProgramasDePersona o p)