module Torneo(
	Torneo,
	equipo,
	equipoDe,
	jugadores,
	equipoGoleador,
	comenzarTorneo,
	equipos,
	registrarGol,
	ingresarJugador,
	sinEquipoGoleador
)where


data Torneo = ConsT 
			 (Map Nombre Equipo) 		-- Relaciona a los equipos con su nombre
			 (Map Nombre Nombre)		-- Relaciona nombres de jugadores (las claves) con nombres de equipos (los valores)
			 (PriorityQueue Equipo)     -- Posee a todos los equipos del torneo, y que permite obtenerlos de forma eficiente de mayor 
			 							-- a menor en base a la cantidad de goles que acertaron

-- a) Invariantes de representacióm
-- Todos los equipos que estan en el segundo map como valores, tienen que estar como claves en el primer map
-- Todos los esquipos de la PriorityQueue tienen que estar en el primer map
-- Un jugador sólo puede pertenecer a un equipo.


-- b) Propósito: dado un nombre de equipo devuelve al equipo con dicho nombre.
-- Eficiencia: O(log N)
-- Puntaje: 0.5
equipo :: Nombre -> Torneo -> Maybe Equipo
equipo nombre (ConstT mapEquipo _ _ ) = lookupM nombre mapEquipo 

-- c) Propósito: dado un nombre de jugador devuelve el equipo en el que juega.
-- Nota: el jugador puede no existir, pero si existe, su equipo también.
-- Eficiencia: O(log N)
-- Puntaje: 0.5
equipoDe :: Nombre -> Torneo -> Maybe Equipo
equipoDe nombre (ConstT _ mapJuga _) = lookupM nombre mapJuga

-- d) Propósito: denota la lista de jugadores del torneo.
-- Eficiencia: O(N)
-- Puntaje: 0.5
jugadores :: Torneo -> [Nombre]
jugadores (ConstT _ mapJuga _) = domM mapJuga

-- e) Propósito: indica el equipo que más goles anotó.
-- Precondición: existe al menos un equipo en el torneo.
-- Eficiencia: O(1)
-- Puntaje: 0.5
equipoGoleador :: Torneo -> Equipo
equipoGoleador (ConstT _ _ pq) = maxPQ pq

-- f) Propósito: devuelve un torneo en el que participan los equipos dados.
-- Nota: los equipos ya poseen jugadores, no olvidar sumarlos a la estructura.
-- Eficiencia: O(N2)
-- Puntaje: 1
comenzarTorneo :: [Equipo] -> Torneo
comenzarTorneo [] = (ConstT emptyM emptyM emptyPQ)
comenzarTorneo equipos = (ConstT (cargarEquipos equipos) (cargarJugadores equipos) (cargarPQ equipos))

-- Costo: O(N log N). Por cada elemento de la lista, se llama a la funcion assocM con costo O(log N)
cargarEquipos :: [Equipo] -> Map Nombre Equipo
cargarEquipos [] = emptyM
cargarEquipos (e:es) = assocM (nombre e) (jugadores e) (cargarEquipos es)

-- Costo: O(N2). Por cada equipo de la lista recorro todos los jugadores del equipo y los voy agregando al map
cargarJugadores :: [Equipo] -> Map Nombre Nombre
cargarJugadores [] = emptyM
cargarJugadores (e:es) = cargarJugadoresAlMap (nombre e) (jugadores e) (cargarJugadores es)

-- Costo: O(N log N). Por cada nombre de jugador de la lista hago assocM con costo O(log N)
cargarJugadoresAlMap :: Nombre -> [Nombre] -> Map Nombre Nombre -> Map Nombre Nombre
cargarJugadoresAlMap _ [] mapJug = mapJug
cargarJugadoresAlMap nombreEqui (x:xs) mapJug = assocM x nombreEqui (cargarJugadoresAlMap nombreEqui xs mapJug)

-- Costo: O(N log N). Por cada elemento de la lista, se llama a la funcion insertPQ con costo O(log N)
cargarPQ :: [Equipo] -> PriorityQueue Equipo
cargarPQ [] = emptyPQ
cargarPQ (e:es) = insertPQ e (cargarPQ es)

-- g) Propósito: denota la lista de equipos del torneo.
-- Eficiencia: O(N log N)
-- Puntaje: 0.5
equipos :: Torneo -> [Equipo]
equipos (ConstT mapEqui _ _) = domM mapEqui

-- h) Propósito: dados un nombre de jugador y un nombre de equipo, ingresa un gol anotado por el jugador dado para el equipo
-- dado.
-- Precondición: existe un jugador y un equipo con dichos nombres.
-- Eficiencia: O(N log N)
-- Puntaje: 1.5
registrarGol :: Nombre -> Nombre -> Torneo -> Torneo
registrarGol nomJug nomEqui (ConstT mapEquipo mapJug pq) = ConstT (sumarGolAJugadorEnMap nomJug nomEqui mapEqui)
																  mapJug
																  (sumarGolAJugadorEnPQ nomJug nomEqui pq)

-- Costo: O(log N). Se llama a las funciones lookupM,assocM y anotarGol, las tres con costo O(log N)
sumarGolAJugadorEnMap :: Nombre -> Nombre -> Map Nombre Equipo -> Map Nombre Equipo
sumarGolAJugadorEnMap nomJug nomEqui mapEqui = case lookupM nomEqui mapEquipo of 
												Nothing -> error "No existe el equipo"
												Just equipo -> assocM nomEqui (anotarGol nomJug equipo) mapEqui

-- Costo: O(N log N). Por cada elemento de la PQ se llama a la funcion insertPQ con costo O(log N)
sumarGolAJugadorEnPQ :: Nombre -> Nombre -> PriorityQueue Equipo -> PriorityQueue Equipo
sumarGolAJugadorEnPQ nomJug nomEqui pq = if nombre (maxPQ pq )  == nomEqui
											then insertPQ (anotarGol nomJug (maxPQ pq)) (deleteMaxPQ pq)
											else insertPQ (maxPQ pq) (sumarGolAJugadorEnPQ nomJug nomEqui (deleteMaxPQ pq))

-- i) Propósito: dado un nombre de jugador y un nombre de equipo, ingresa al torneo dicho jugador, con cero goles, agregándolo
-- al equipo dado.
-- Eficiencia: O(N log N)
-- Puntaje: 1.5
ingresarJugador :: Nombre -> Nombre -> Torneo -> Torneo
ingresarJugador nomJug nomEqui (ConstT mapEquipo mapJuga pq) = ConstT (buscarEquipoYFicharJugadorEnMap nomJug nomEqui mapEquipo) 
																	  (assocM nomJug nomEqui mapJuga)
																	  (buscarEquipoYFicharJugadorEnPQ nomJug nomEqui pq)

-- Costo: O(log N). Se llama a las funciones lookupM,assocM y fichar, las tres con costo O(log N)
buscarEquipoYFicharJugadorEnMap :: Nombre -> Nombre -> Map Nombre Equipo -> Map Nombre Equipo
buscarEquipoYFicharJugadorEnMap nomJug nomEqui mapEquipo = case lookupM nomEqui mapEquipo of
															Nothing -> error "El equipo no existe"
															Just equipo = assocM nomEqui (fichar nomJug equipo) mapEquipo

-- Costo: O(N log N). Por cada elemento de la PQ se llama a la funcion insertPQ con costo O(log N)
buscarEquipoYFicharJugadorEnPQ :: Nombre -> Nombre -> PriorityQueue Equipo-> PriorityQueue Equipo
buscarEquipoYFicharJugadorEnPQ nomJug nomEqui pq = if nombre (maxPQ pq )  == nomEqui
													then insertPQ (fichar nomJug (maxPQ pq)) (deleteMaxPQ pq)
													else insertPQ (maxPQ pq) (buscarEquipoYFicharJugadorEnPQ nomJug nomEqui (deleteMaxPQ pq))

-- j) Propósito: devuelve un torneo donde se ha quitado al equipo con más goles anotados.
-- Eficiencia: O(N log N)
-- Puntaje: 1.5
sinEquipoGoleador :: Torneo -> Torneo
sinEquipoGoleador (ConstT mapEquipo mapJuga pq) = let equipo = maxPQ pq in
													(ConstT (deleteM (nombre equipo) mapEquipo)
															(eliminarJugadoresDelEquipo (jugadores equipo) mapJuga)
															(deleteMaxPQ pq))

-- Costo: O(N log N). Por cada elemento de la lista de nombres se llama a la funcion deleteM con costo O(log N)
eliminarJugadoresDelEquipo :: [Nombre] -> Map Nombre Nombre -> Map Nombre Nombre
eliminarJugadoresDelEquipo [] mapJug = mapJug
eliminarJugadoresDelEquipo (j:js) mapJug = eliminarJugadoresDelEquipo js (deleteM j mapJug)

-- --------------------------------------------------------------------------------------------------------------------
-- Funciones de usuario -----------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------------------------
-- k) Propósito: dada una lista de pares de nombre de jugador (primera componente) y nombre de equipo (segunda componente),
-- anota un gol en el torneo por cada elemento en la lista.
-- Puntaje: 0.5
-- Costo: O(N2 log N). Por cada elemento de la lista (N) llamo a la funcion registrarGol con costo N log N
anotarGoles :: [(Nombre, Nombre)] -> Torneo -> Torneo
anotarGoles [] tornero = tornero
anotarGoles (xs:xss) tornero = anotarGoles xs (registrarGol (fst xs) (snd xs) torneo)


-- l) Propósito: dado un número n denota a los n equipos más goleadores, ordenados por cantidad de goles de mayor a menor.
-- Precondición: existen al menos n equipos en el torneo.
-- Eficiencia: O(N log N)
-- Puntaje: 0.75
mejoresEquipos :: Int -> Torneo -> [Equipo]
mejoresEquipos 0 _ = []
mejoresEquipos n torneto = (equipoGoleador torneto) : (mejoresEquipos (n-1) (sinEquipoGoleador torneto))


-- m) Propósito: denota la lista de jugadores del torneo junto con sus respectivos goles.
-- Eficiencia: O(N log N)
-- Puntaje: 0.75
jugadoresYGoles :: Torneo -> [(Nombre, Int)]
jugadoresYGoles torneo = jugadoresYGoles' (jugadores torneo) torneo

-- Costo: O(N log N). Por cada elemento de la lista llamo a dos funciones (equipoDe golesDe) con costo O(log N). Por lo que el costo seria
-- N (log N + log N) = N (2 log N) = N log N
jugadoresYGoles' :: [Nombre] -> Torneo -> [(Nombre, Int)]
jugadoresYGoles' [] _ = []
jugadoresYGoles' (j:js) torneo = case equipoDe j of
									Nothing -> (jugadoresYGoles' js torneo)
									Just equipo -> (j, (golesDe j equipo)) : (jugadoresYGoles' js torneo)

-- --------------------------------------------------------------------------------------------------------------------
-- Representación -----------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------------------------
-- n) Dar una posible representación para el tipo Equipo, de manera de que se pueda cumplir con el orden dado para cada
-- operación de la interfaz, pero sin implementarlas.
-- Puntaje: 0.5

data Equipo = ConsE Nombre [Nombre] (Map Nombre Int)

-- La implementacion tendrá:
-- Nombre: nombre del equipos
-- [Nombre]: lista de los nombres de los jugadores del equipo
-- Map Nombre Int: map que relaciona el nombre de cada jugador del equipo con la cantidad de goles que hizo