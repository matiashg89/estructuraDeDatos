import EscuelaDeMagia

data Mago = M String (Set Hechizo)

-- Propósito: Retorna todos los hechizos aprendidos por los magos.
-- Eficiencia: O(M ∗ (log M + H log H))
hechizosAprendidos :: EscuelaDeMagia -> Set Hechizo
hechizosAprendidos edm = todosLosEchizosDe (magos edm) edm

todosLosEchizosDe :: [Mago] -> EscuelaDeMagia -> Set Hechizo
todosLosEchizosDe [] _ = emptyS
todosLosEchizosDe (m:ms) edm = unionS (hechizosDe m) (todosLosEchizosDe ms edm)

-- Propósito: Indica si existe un mago que sabe todos los hechizos enseñados por la escuela.
-- Eficiencia: O(log M)
hayUnExperto :: EscuelaDeMagia -> Bool
hayUnExperto emd = hayUnExperto' (magos edm) edm

hayUnExperto' :: [Mago] -> EscuelaDeMagia -> Bool
hayUnExperto' [] _ = False
hayUnExperto' (m:ms) edm = ((leFaltanAprender (nombre m) edm) < 0) || (hayUnExperto ms edm)

-- Propósito: Devuelve un par con la lista de magos que saben todos los hechizos dados por la escuela y la escuela sin dichos magos.
-- Eficiencia: O(M log M)
egresarExpertos :: EscuelaDeMagia -> ([Mago], EscuelaDeMagia)
egresarExpertos edm = egresarExpertos' (magos edm) edm


egresarExpertos' :: [Mago] -> EscuelaDeMagia -> ([Mago], EscuelaDeMagia)
egresarExpertos' (m:ms) edm = if leFaltanAprender (nombre m) edm <= 0 
								then     (egresarExpertos' ms edm)
								else 