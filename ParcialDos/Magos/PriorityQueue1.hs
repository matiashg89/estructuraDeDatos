module PriorityQueue1(
	PriorityQueue,emptyPQ,isEmptyPQ,findMinPQ,deleteMinPQ
) where

data (Ord a) => PriorityQueue a = PQ [a] deriving Show


-- Propósito: devuelve una priority queue vacía.
emptyPQ :: (Ord a) => PriorityQueue a
emptyPQ = PQ []

-- Propósito: indica si la priority queue está vacía.
isEmptyPQ :: (Ord a) => PriorityQueue a -> Bool
isEmptyPQ (PQ lista) = null lista

-- Propósito: inserta un elemento en la priority queue.
insertPQ :: Ord a => a -> PriorityQueue a -> PriorityQueue a
insertPQ elemento (PQ lista) = PQ (lista ++ [elemento])

-- Propósito: devuelve el elemento más prioriotario (el mínimo) de la priority queue.
-- Precondición: parcial en caso de priority queue vacía.
findMinPQ :: Ord a => PriorityQueue a -> a
findMinPQ (PQ lista) = minimoDeLaLista lista

minimoDeLaLista :: Ord a => [a] -> a
minimoDeLaLista [a] = a
minimoDeLaLista (x:xs) = min x (minimoDeLaLista xs)

-- Propósito: devuelve una priority queue sin el elemento más prioritario (el mínimo).
-- Precondición: parcial en caso de priority queue vacía.
deleteMinPQ :: Ord a => PriorityQueue a -> PriorityQueue a
deleteMinPQ (PQ lista) = PQ (eliminarElementoDeLaLista (minimoDeLaLista lista) lista)

eliminarElementoDeLaLista :: Ord a => a -> [a] -> [a]
eliminarElementoDeLaLista _ [] = []
eliminarElementoDeLaLista a (x:xs) = if a == x
										then xs
										else x : (eliminarElementoDeLaLista a xs)