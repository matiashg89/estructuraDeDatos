module EscuelaDeMagia1(
	EscuelaDeMagia,
	fundarEscuela,
	estaVacia,
	registrar,
	magos,
	hechizosDe,
	leFaltanAprender,
	egresarUno,
	enseniar
) where

import PriorityQueue1

data EscuelaDeMagia = EDM (Set Hechizo) (Map Nombre Mago) (PriorityQueue Mago)

-- Invariantes de reprensentacion
-- Todos los magos del tercer map tienen que estar en el segundo y todos los magos del map tiene que estar en la PQ
-- Todos los Hechizos de cada mago tienen que estar en el primer Set
-- En la escuela no existen dos magos con el mismo nombre.

-- Propósito: Devuelve una escuela vacía.
-- Eficiencia: O(1)
fundarEscuela :: EscuelaDeMagia
fundarEscuela = EDM emptyS emptyM emptyPQ

-- Propósito: Indica si la escuela está vacía.
-- Eficiencia: O(1)
estaVacia :: EscuelaDeMagia -> Bool
estaVacia EDM _ _ mapPQ = isEmptyPQ mapPQ

-- Propósito: Incorpora un mago a la escuela (si ya existe no hace nada).
-- Eficiencia: O(log M)
registrar :: Nombre -> EscuelaDeMagia -> EscuelaDeMagia
registrar name (EDM setHechizos mapMagos pqMagos) = case lookupM name mapMagos of 
														Nothing   -> EDM setHechizos (agregarMagoAlMap mapMagos name) (agregarMagoAlPQ pqMagos name)
														Just mago -> EDM setHechizos mapMagos pqMagos

-- En este punto ya se que el mago no existe en el map asi que simplemente lo asocio
agregarMagoAlMap :: Map Nombre Mago -> String -> (Map Nombre Mago)
agregarMagoAlMap mapMagos nombre = assocM nombre (crearM nombre) mapMagos 

-- En este punto ya se que el mago no existe en la PQ asi que simplemente lo agrego
agregarMagoAlPQ :: PriorityQueue Mago -> String -> PriorityQueue Mago
agregarMagoAlPQ pqMagos nombre = insertPQ (crearM nombre) pqMagos

-- Propósito: Devuelve los nombres de los magos registrados en la escuela.
-- Eficiencia: O(M)
magos :: EscuelaDeMagia -> [Nombre]
magos (EDM setHechizos mapMagos pqMagos) = domM mapMagos

-- Propósito: Devuelve los hechizos que conoce un mago dado.
-- Precondición: Existe un mago con dicho nombre.
-- Eficiencia: O(log M)
hechizosDe :: Nombre -> EscuelaDeMagia -> Set Hechizo
hechizosDe nombre (EDM setHechizos mapMagos pqMagos) = case lookupM name mapMagos of 
														Nothing   -> error ("El mago no existe")
														Just mago -> hechizos mago

-- Propósito: Dado un mago, indica la cantidad de hechizos que la escuela ha dado y él no sabe.
-- Precondición: Existe un mago con dicho nombre.
-- Eficiencia: O(log M)
leFaltanAprender :: Nombre -> EscuelaDeMagia -> Int
leFaltanAprender nombre escuela = (cantidedDeHechizosDeLaEscuela escuela) - sizeS (hechizosDe nombre escuela)

cantidedDeHechizosDeLaEscuela :: EscuelaDeMagia -> Int
cantidedDeHechizosDeLaEscuela (EDM setHechizos mapMagos pqMagos) = sizes setHechizos

-- Propósito: Devuelve el mago que más hechizos sabe y la escuela sin dicho mago.
-- Precondición: Hay al menos un mago.
-- Eficiencia: O(log M)
egresarUno :: EscuelaDeMagia -> (Mago, EscuelaDeMagia)
egresarUno (EDM setHechizos mapMagos pqMagos) = case maxPQ pqMagos
												 mago -> (mago,EDM(setHechizos (eliminarMagoDelMap mago mapMagos) (deleteMaxPQ pqMagos)))

eliminarMagoDelMap :: Mago -> Map Nombre Mago
eliminarMagoDelMap mago mapMagos = deleteM (nombre mago) mapMagos

-- Propósito: Enseña un hechizo a un mago existente, y si el hechizo no existe en la escuela es incorporado a la misma.
-- Nota: No importa si el mago ya conoce el hechizo dado.
-- Precondición: Existe un mago con dicho nombre.
-- Eficiencia: O(M log M + log H)
enseniar :: Hechizo -> Nombre -> EscuelaDeMagia -> EscuelaDeMagia
enseniar hechizo nombreMago (EDM setHechizos mapMagos pqMagos) = 
									let mago = (enseniarHechizoAMago hechizo nombreMago mapMagos) in
										if belongsS hechizo setHechizos
											then (EDM setHechizos (assocM nombreMago mago mapMagos) (reemplazarEnPq pqMagos mago))
											else (EDM (addS hechizo setHechizos) (assocM nombreMago mago mapMagos) (reemplazarEnPq pqMagos mago))

enseniarHechizoAMago :: Hechizo -> String -> Map Nombre Mago -> Map Nombre Mago
enseniarHechizoAMago hechizo nombreMago mapMagos = case lookupM nombreMago mapMagos of
													Nothing -> error "Mago no encontrado" 
													Just mago = aprender hechizo  mago

-- DUDA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
reemplazarEnPq :: PriorityQueue Mago -> Mago -> PriorityQueue Mago
reemplazarEnPq = error ("No implementado")