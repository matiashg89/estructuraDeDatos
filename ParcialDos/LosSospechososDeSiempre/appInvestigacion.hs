-- k) Propósito: Comienza una investigación con una lista de nombres sin evidencia.
-- Puntaje: 0.5
comenzarConPersonas :: [Nombre] -> Investigacion
comenzarConPersonas [] = comenzarInvestigacion
comenzarConPersonas personas = ingresarPersonas personas comenzarInvestigacion 

-- l) Propósito: Indica si las personas en la investigación son todas inocentes.
-- Puntaje: 0.75
todosInocentes :: Investigacion -> Bool
todosInocentes inv = verificarSospechosos (nombresIngresados inv)

verificarSospechosos :: [Nombre] -> Bool
verificarSospechosos [] = True
verificarSospechosos (x:xs) = (not esSospechoso x) && (verificarSospechosos xs)

-- m) Propósito: Indica si la evidencia en la lista es suficiente para cerrar el caso.
-- Puntaje: 1
terminaCerrado :: [(Evidencia, Nombre)] -> Investigacion -> Bool
terminaCerrado [] inv = casoCerrado inv
terminaCerrado evidencias inv = casoCerrado (agregarEvidencias evidencias inv)

agregarEvidencias :: [(Evidencia, Nombre)] -> Investigacion -> Investigacion
agregarEvidencias [] inv = inv
agregarEvidencias (x:xs) inv = agregarEvidencias xs (ingresarEvidencia (fst x) (snd x) inv)

data Peronsa = P String [Evidencia] Int