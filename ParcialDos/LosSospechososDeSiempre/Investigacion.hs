
module Investigacion(
	Investigacion
) where

data Investigacion = ConsI 	(Map Nombre Persona)
							(Map Evidencia [Nombre])
							(PriorityQueue Persona)
							Int

-- Invariantes de reperenstacion
-- Todas las personas que se relacionan en los dos primeros maps tienen que formar parte de la PQ y viceversa
-- Dos personas son iguales si poseen el mismo nombre
-- Una persona es más sospechosa que otra si posee más evidencia en su contra
-- El int nunca puede ser menor a cero

-- b) Propósito: crea una investigación sin datos.
-- Eficiencia: O(1)
-- Puntaje: 0.25
comenzarInvestigacion :: Investigacion
comenzarInvestigacion = comenzarInvestigacion emptyM emptyM emptPQ 0

-- c) Propósito: devuelve la cantidad de eviencia ingresada.
-- Eficiencia: O(1)
-- Puntaje: 0.25
cantEvidenciaIngresada :: Investigacion -> Int
cantEvidenciaIngresada (ConsI _ _ _ e) = e

-- d) Propósito: devuelve la evidencia ingresada.
-- Eficiencia: O(N)
-- Puntaje: 0.5
evidenciaIngresada :: Investigacion -> [Evidencia]
evidenciaIngresada (ConsI _ mapEvidencia _ _) = domM mapEvidencia

-- e) Propósito: devuelve los nombres de personas ingresadas.
-- Eficiencia: O(N)
-- Puntaje: 0.5
nombresIngresados :: Investigacion -> [Nombre]
nombresIngresados (ConsI mapPersonas _ _ _) = domM mapPersonas

-- f) Propósito: indica si la investigación posee al menos una persona con 5 evidencias en su contra.
-- Eficiencia: O(1)
-- Puntaje: 0.5
casoCerrado :: Investigacion -> Bool
casoCerrado (ConsI _ _ pq _) = (cantEvidencia (maxPQ pq)) >= 5

-- g) Propósito: indica si esa persona tiene al menos una evidencia en su contra.
-- Nota: la persona puede no existir.
-- Eficiencia: O(log N)
-- Puntaje: 0.5
esSospechoso :: Nombre -> Investigacion -> Bool
esSospechoso persona (ConsI mapPersonas mapEvidencia _ _) = case lookupM persona mapPersonas of
																Nothing -> False
																Just persona = cantEvidencia personas >= 1

-- h) Propósito: devuelve a las personas con cero evidencia en su contra.
-- Eficiencia: O(N log N)
-- Puntaje: 0.5
posiblesInocentes :: Investigacion -> [Persona]
posiblesInocentes (ConsI _ _ pqPersonas _) = personasConCeroEvidencias pqPersonas

personasConCeroEvidencias :: PriorityQueue Persona -> [Persona]
personasConCeroEvidencias emptPQ = []
personasConCeroEvidencias pq = let maxPersona = maxPQ pq in
								if cantEvidencia maxPersona == 0
								 then [maxPersona] ++ (personasConCeroEvidencias deleteMaxPQ(pq))
								 else (personasConCeroEvidencias deleteMaxPQ(pq))


-- i) Propósito: ingresa a personas nuevas a la investigación (mediante sus nombres), sin evidencia en su contra.
-- Precondición: las personas no existen en la investigación y no hay nombres repetidos.
-- Eficiencia: O(N log N)
-- Puntaje: 1.75
ingresarPersonas :: [Nombre] -> Investigacion -> Investigacion
ingresarPersonas [] inv = inv
ingresarPersonas personas (ConsI mapPersonas mapEvidencias pqPersonas cant) = ConsI (agregarPersonasAMap mapPersonas personas) 
																					mapEvidencias
																					(agregarPersonasAPQ pqPersonas personas) 
																					cant

agregarPersonasAMap :: Map Nombre Persona -> [Nombre] -> Map Nombre Persona
agregarPersonasAMap mapP []  = mapP
agregarPersonasAMap mapP (p:ps) = agregarPersonasAMap (assocM (crearP p) mapP) ps

agregarPersonasAPQ :: PriorityQueue Persona -> [Nombre] -> PriorityQueue Persona
agregarPersonasAPQ pq [] = pq
agregarPersonasAPQ pq (p:ps) = agregarPersonasAPQ (insertPQ p pq) ps


data Investigacion = ConsI 	(Map Nombre Persona)
							(Map Evidencia [Nombre])
							(PriorityQueue Persona)
							Int


-- j) Propósito: asocia una evidencia a una persona dada.
-- Precondición: la evidencia aún no está asociada a esa persona.
-- Nota: la persona y la evidencia existen, pero NO están asociadas.
-- Eficiencia: O(N log N)
-- Puntaje: 1.75
ingresarEvidencia :: Evidencia -> Nombre -> Investigacion -> Investigacion
ingresarEvidencia evidencia nombre (ConsI mapPersonas mapEvidencias pqPersonas cant) = ConsI (agregarEvicenciaAPersonaEnMap evidencia nombre mapPersonas)
																							 (asociarEvidenciaANombre mapEvidencias evidencia nombre)
																							 (incrmentarPQ pqPersonas evidencia nombre)
																							 (cant + 1)


agregarEvicenciaAPersonaEnMap :: Evidencia -> String -> Map Nombre Persona -> Map Nombre Persona
agregarEvicenciaAPersonaEnMap evidencia nombre mapPersona = case lookupM nombre mapPersonas of
																Nothing -> error "No existe la persona"
																Just persona = assocM nombre (agregarEvidencia Evidencia persona) mapPersona

asociarEvidenciaANombre :: Map Evidencia [Nombre] -> Evidencia -> String -> Map Evidencia [Nombre]
asociarEvidenciaANombre mapEvidencias evidencia nombrePersona = case lookupM evidencia mapEvidencias of
																	Nothind -> error "No existe la evidencia"																
																	Just nombres = assocM evidencia ([agregarEvidencia (crearP nombrePersona) evidencia] ++ nombres) mapEvidencias


incrmentarPQ :: PriorityQueue Persona Evidencia -> String ->PriorityQueue Persona
incrmentarPQ = error "sin implementar"