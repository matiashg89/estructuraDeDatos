module RAList(
	RAList,
	emptyRAL,
	isEmptyRAL,
	lengthRAL,
	get,
	minRAL,
	add,
	elems,
	remove,
	set,
	addAt
) where

data RAList a = MKR Int (Map Int a) (Heap a)

-- Invariantes de representacion
-- Si la lista esta vacia el primer int es Cero
-- Todos los valores que se encuentran en el map tienen que estar en el heap
-- Todos los valroes que se encuenran en el heap tienen que estar en el map
-- La cantidad de elementos del map y del heap tiene que ser igual
-- La cantidad de elementos del map y del heap tiene que ser igual al int menos uno


-- a) Propósito: devuelve una lista vacía.
-- Eficiencia: O(1).
emptyRAL :: RAList a
emptyRAL = MKR 0 emptyM emptyH

-- b) Propósito: indica si la lista está vacía.
-- Eficiencia: O(1).
isEmptyRAL :: RAList a -> Bool
isEmptyRAL (MKR pos _ _ ) = pos == 0

-- c) Propósito: devuelve la cantidad de elementos.
-- Eficiencia: O(1).
lengthRAL :: RAList a -> Int
lengthRAL (MKR pos _ _ ) = pos - 1

-- d) Propósito: devuelve el elemento en el índice dado.
-- Precondición: el índice debe existir.
-- Eficiencia: O(log N).
get :: Int -> RAList a -> a
get (MKR pos mapElem _ ) = lookupM pos mapElem

-- e) Propósito: devuelve el mínimo elemento de la lista.
-- Precondición: la lista no está vacía.
-- Eficiencia: O(1).
minRAL :: Ord a => RAList a -> a
minRAL (MKR pos _ heapElem ) = findMin heapElem

-- f) Propósito: agrega un elemento al final de la lista.
-- Eficiencia: O(log N).
add :: Ord a => a -> RAList a -> RAList a
add elemento (MKR pos mapElem heapElem ) = MKR (pos + 1) (assocM (pos + 1) elemento mapElem ) (insertH elemento heapElem)

-- g) Propósito: transforma una RAList en una lista, respetando el orden de los elementos.
-- Eficiencia: O(N log N).
elems :: Ord a => RAList a -> [a]
elems (MKR _ _ heapElem ) = pasarHeapALista heapElem

-- Costo O(h log h) siendo h la cantidad de elementos del heap
pasarHeapALista :: Ord a => Heap a -> [a]
pasarHeapALista emptyH = []
pasarHeapALista heap = (findMin heap) : (pasarHeapALista (deleteMin heap))

-- h) Propósito: elimina el último elemento de la lista.
-- Precondición: la lista no está vacía.
-- Eficiencia: O(N log N).
remove :: Ord a => RAList a -> RAList a
remove (MKR pos mapElem heapElem ) = MKR (pos - 1) (deleteM (pos-1) mapElem) (eliminarUltimoElementoDelHeap (pos - 1) heapElem)

-- Costo O(N log H): Siendo N el numero a buscar y H la cantidad de elementos del HEAP... es decir, va a hacer un recorrido sobre el numero,
-- hasta encontrar la posicion a eliminar (ultimo elemento), y al mismo tiempo va a ir eliminando el minimo
eliminarUltimoElementoDelHeap :: Ord a => a => Heap a -> Heap a
eliminarUltimoElementoDelHeap _ emptyH = emptyH
eliminarUltimoElementoDelHeap 1 heap = heap
eliminarUltimoElementoDelHeap n heap = insertH (findMin heap) (eliminarUltimoElementoDelHeap (n-1) (deleteMin heap))

-- i) Propósito: reemplaza el elemento en la posición dada.
-- Precondición: el índice debe existir.
-- Eficiencia: O(N log N).
set :: Ord a => Int -> a -> RAList a -> RAList a
set pos elemen (MKR p mapElem heapElem ) = MKR p (assocM pos elemen mapElem) (reemplazarElementoEnHeap pos elemen heapElem)

reemplazarElementoEnHeap :: Ord a => Int -> a -> Heap a -> Heap a
reemplazarElementoEnHeap pos elemen heap = if isEmptyH heap
												then emptyH
												else if elemen == (findMin heap)
													then insertH (findMin heap) heap
													else insertH x (reemplazarElementoEnHeap pos elemen (deleteMin heap))

-- f h =
-- 	if isEmptyH h
-- 		then emptyH
-- 		else if m == x
-- 			then insertH x h
-- 			else insertH m (f deleteMin h)

-- j) Propósito: agrega un elemento en la posición dada.
-- Precondición: el índice debe estar entre 0 y la longitud de la lista.
-- Observación: cada elemento en una posición posterior a la dada pasa a estar en su posición siguiente.
-- Eficiencia: O(N log N).
-- Sugerencia: definir una subtarea que corra los elementos del Map en una posición a partir de una posición dada. Pasar
-- también como argumento la máxima posición posible.
addAt :: Ord a => Int -> a -> RAList a -> RAList a
addAt = error "Falta implemntar"


-- RAList
-- 	- 3
-- 	- {(1,x),(2,y),(3,z)}
-- 	- {y,z,x}

-- addAt :: Int -> a -> R -> R
-- 	- m: 2
-- 	- v: w

-- 3 .. 2
-- (1,x),(2,w),(3,y),(4,z)

-- RAList
-- 	- 4
-- 	- {(1,x),(2,w),(3,y),(4,z)}
-- 	- {y,z,x,w}


corrimientoEInsercion :: Ord a => Int -> Int -> a -> Map Int a -> Map Int a
corrimientoEInsercion base actual x map = 
		if base > actual
			then assocM actual x map
			else corrimientoEInsercion
					base 
					(actual - 1)
					(assocM (actual + 1) (fromJust (lookupM map actual)) map)