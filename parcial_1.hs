-- Funciones genericas
sinRepetidos :: Eq a => [a] -> [a] 
sinRepetidos [] = []
sinRepetidos (x:xs) = if estaElElementoEnLaLista x xs
						then sinRepetidos xs
						else x : (sinRepetidos xs)

estaElElementoEnLaLista :: Eq a => a -> [a] -> Bool
estaElElementoEnLaLista c [] = False
estaElElementoEnLaLista c (x:xs) = (c == x) || (estaElElementoEnLaLista c xs)

agregarA :: a -> [[a]] -> [[a]]
agregarA n []  = []
agregarA n (x:xs) =
	(n : x) : agregarA n xs

-- Torres
data Color = Azul | Verde | Rojo deriving (Show,Eq)
data Torre = Base | Bloque Color Torre deriving Show

miTorre :: Torre
miTorre = Bloque Azul 
			(Bloque Verde 
				(Bloque Rojo (
					Base)))
-- a) Propósito: dada una torre, indica la cantidad de bloques que contiene
cantidadDeBloques :: Torre -> Int
cantidadDeBloques Base = 0
cantidadDeBloques (Bloque _ torre) = 1 + (cantidadDeBloques torre)

-- b) Propósito: dados un color c y una torre, indica si todos los bloques de la torre son de color c
todos :: Color -> Torre -> Bool
todos col Base = True
todos col (Bloque color torre) = (color == col) && (todos col torre)


-- c) Propósito: dados un número n, un color c y una torre, agrega n bloques de cemento de color c a la torre, luego del primer 
-- bloque de color c.
-- Precondición: hay al menos un bloque de color c.
agregarOtrosN :: Int -> Color -> Torre -> Torre
agregarOtrosN 0 col torre = torre
agregarOtrosN cant color Base = Base
agregarOtrosN cant col (Bloque color torre) = Bloque color (if col == color 
															then (agregarNColores cant color torre)
															else (agregarOtrosN cant col torre))

agregarNColores :: Int -> Color -> Torre -> Torre
agregarNColores 0 col torre = torre
agregarNColores cant col torre = Bloque col (agregarNColores (cant-1) col torre)

-- d) Propósito: dada una lista de colores y una torre, quita de la torre todos los bloques cuyo color sea alguno de los de la lista
sinColores :: [Color] -> Torre -> Torre
sinColores [] torre = torre
sinColores lista Base = Base
sinColores lista (Bloque color torre) = if estaElColorEnLaLista color lista
											then (sinColores lista torre)
											else (Bloque color (sinColores lista torre))

-- Propósito: Indica si el color se encuentra en la lista de colores
estaElColorEnLaLista :: Color -> [Color] -> Bool
estaElColorEnLaLista c [] = False
estaElColorEnLaLista c (x:xs) = (c == x) || (estaElColorEnLaLista c xs)

-- e) Propósito: dada una torre, denota la lista de pares donde el primer elemento es un color y el segundo elemento es la
-- cantidad de veces que aparece dicho color.
-- Nota: si el color no aparece, no hace falta que esté en la lista
aparicionesDeColores :: Torre -> [(Color,Int)]
aparicionesDeColores Base = []
aparicionesDeColores torre = contarApariciones (coloresSinRepetir torre) torre

contarApariciones :: [Color] -> Torre -> [(Color,Int)]
contarApariciones [] _ = []
contarApariciones _ Base = []
contarApariciones (x:xs) torre =  [(x,(aparicionesDe x torre))] ++ (contarApariciones xs torre)

aparicionesDe :: Color -> Torre -> Int
aparicionesDe c Base = 0
aparicionesDe color (Bloque c torre) = if color == c
										then 1 + (aparicionesDe color torre)
										else (aparicionesDe color torre)

coloresSinRepetir :: Torre -> [Color]
coloresSinRepetir Base = []
coloresSinRepetir torre = sinRepetidos(todosLosColores torre)

todosLosColores :: Torre -> [Color] 
todosLosColores Base = []
todosLosColores (Bloque color torre) = [color] ++ (todosLosColores torre)

-- Escenarios
type Energia = Int
type Clave = Int
data Direccion = Izquierda | Centro | Derecha deriving Show
data Dispositivo = Dispositivo Energia [Clave] deriving Show
data Escenario = Acceso Clave | Pared Dispositivo | Punto Dispositivo Escenario Escenario Escenario deriving Show

miEsc :: Escenario
miEsc = Punto (Dispositivo 30 [5,23,65])
			(Pared (Dispositivo 50 [5,15,23,54]))
			(Punto (Dispositivo 90 []) 
				(Acceso 34)
				(Acceso 34)
				(Acceso 34)
			)
			(Punto (Dispositivo 43 []) 
				(Acceso 34)
				(Acceso 34)
				(Acceso 34)
			)
		
-- a) Propósito: dado un escenario, denota la cantidad de accesos que contiene
cantidadDeAccesos :: Escenario -> Int
cantidadDeAccesos (Acceso _ )= 1
cantidadDeAccesos (Pared _) = 0
cantidadDeAccesos (Punto _ e1 e2 e3) =  (cantidadDeAccesos e1) + (cantidadDeAccesos e2) + (cantidadDeAccesos e3)

-- b) Propósito: dado un escenario, denota la lista sin repetidos de todas las claves que hay en él.
todasLasClaves :: Escenario -> [Clave]
todasLasClaves e = sinRepetidos(obtenerTodasLasClaves e)

obtenerTodasLasClaves :: Escenario -> [Clave]
obtenerTodasLasClaves (Pared dispositivo) = (clavesDispositivo dispositivo)
obtenerTodasLasClaves (Acceso clave) = [clave] 
obtenerTodasLasClaves (Punto dispositivo e1 e2 e3) = (clavesDispositivo dispositivo) ++ 
													 (todasLasClaves e1) ++
													 (todasLasClaves e2) ++
													 (todasLasClaves e3)

-- Propósito: Obtiene las claves de un dispositivo
clavesDispositivo :: Dispositivo -> [Clave]
clavesDispositivo (Dispositivo e c) = c

-- c) Propósito: dada una cantidad de energía y un escenario, indica si es posible pasar de escenario con esa cantidad energía.
-- En este ejercicio entiendo que encontrando el primer punto ya puedo salir. Si llego a una pared, supongo que por ese camino no puedo 
-- salir y tengo que buscar otro, por mas que tenga energia disponible
sePuedeSalirCon :: Energia -> Escenario -> Bool
sePuedeSalirCon energia (Acceso _) = True
sePuedeSalirCon energia (Pared dispositivo) = False
sePuedeSalirCon energia (Punto dispositivo e1 e2 e3) = ((energiaDispositivo dispositivo) <= energia) &&
													   (
													   		(sePuedeSalirCon (energia-(energiaDispositivo dispositivo)) e1) ||
															(sePuedeSalirCon (energia-(energiaDispositivo dispositivo)) e2) || 
													   		(sePuedeSalirCon (energia-(energiaDispositivo dispositivo)) e3)
													   	)

energiaDispositivo :: Dispositivo -> Energia
energiaDispositivo (Dispositivo e _) = e

-- d) Propósito: dado un camino y un escenario, indica si siguiendo el camino se pasa de escenario.
-- Precondición: el camino es válido en el escenario.
-- Nota: se asume suficiente cantidad de energía.
caminoGanador :: [Direccion] -> Escenario -> Bool
caminoGanador [] (Acceso _) = True
caminoGanador (x:xs) (Acceso _) = True
caminoGanador [] e = False
caminoGanador (x:xs) (Pared dispositivo) = False
caminoGanador (x:xs) (Punto dispositivo e1 e2 e3) = if esMismaDireccion x Izquierda 
														then caminoGanador xs e1
														else if esMismaDireccion x Derecha
															then caminoGanador xs e3
															else caminoGanador xs e2	

esMismaDireccion :: Direccion -> Direccion -> Bool
esMismaDireccion Izquierda Izquierda = True
esMismaDireccion Derecha Derecha = True
esMismaDireccion Centro Centro = True
esMismaDireccion _ _ = False

-- e) Propósito: dado un escenario, indica todos los caminos que pueden tomarse en él.
-- Nota: un camino puede terminar en un acceso o en una pared.
todosLosCaminos :: Escenario -> [[Direccion]]
todosLosCaminos (Acceso _) = [[]]
todosLosCaminos (Pared dispositivo) = [[]]
todosLosCaminos (Punto dispositivo e1 e2 e3) = 	(agregarA Izquierda (todosLosCaminos e1))
												++
												(agregarA Centro (todosLosCaminos e2))
												++
												(agregarA Derecha (todosLosCaminos e3))