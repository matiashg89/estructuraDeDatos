-- 1. Recursión sobre listas
-- 1. Dada una lista de enteros devuelve la suma de todos sus elementos.
sumatoria :: [Int] -> Int
sumatoria [] = 0
sumatoria (x:xs) = x + sumatoria xs

-- 2. Dada una lista de elementos de algún tipo devuelve el largo de esa lista, es decir, la cantidad de elementos que posee.
longitud :: [a] -> Int
longitud [] = 0
longitud (x:xs) = 1 + longitud xs

--3. Dada una lista de enteros, devuelve la lista de los sucesores de cada entero.
sucesores :: [Int] -> [Int]
sucesores [] = []
sucesores (x:xs) = (x+1) : sucesores xs

-- 4. Dada una lista de booleanos devuelve True si todos sus elementos son True.
conjuncion :: [Bool] -> Bool
conjuncion [] = True
conjuncion (x:xs) = x && conjuncion xs

--5. Dada una lista de booleanos devuelve True si alguno de sus elementos es True.
disyuncion :: [Bool] -> Bool
disyuncion [] = False
disyuncion (x:xs) = x || disyuncion xs

-- 6. Dada una lista de listas, devuelve una única lista con todos sus elementos.
aplanar :: [[a]] -> [a]
aplanar [] = []
aplanar (xs:xss) = xs ++ (aplanar xss)

--7. Dados un elemento e y una lista xs devuelve True si existe un elemento en xs que sea igual a e.
pertenece :: Eq a => a -> [a] -> Bool
pertenece e [] = False
pertenece e (x:xs) = x == e || (pertenece e xs)

--8. Dados un elemento e y una lista xs cuenta la cantidad de apariciones de e en xs.
apariciones :: Eq a => a -> [a] -> Int
apariciones e [] = 0
apariciones e (x:xs) = if e == x then 1 + (apariciones e xs) else (apariciones e xs)

--9. Dados un número n y una lista xs, devuelve todos los elementos de xs que son menores a n.
losMenoresA :: Int -> [Int] -> [Int]
losMenoresA i [] = []
losMenoresA i (x:xs) = if x < i then (x : (losMenoresA i xs)) else (losMenoresA i xs)

--10. Dados un número n y una lista de listas, devuelve la lista de aquellas listas que tienen más de n elementos.
lasDeLongitudMayorA :: Int -> [[a]] -> [[a]]
lasDeLongitudMayorA i [] = []
lasDeLongitudMayorA i (xs:xss) = 
		if length xs > i
		then xs : (lasDeLongitudMayorA i xss)
		else (lasDeLongitudMayorA i xss)

--11. Dados una lista y un elemento, devuelve una lista con ese elemento agregado al final de la lista.
agregarAlFinal :: [a] -> a -> [a]
agregarAlFinal [] a = [a]
agregarAlFinal (x:xs) a = x : (agregarAlFinal xs a)

--12. Dadas dos listas devuelve la lista con todos los elementos de la primera lista y todos los elementos de la segunda a continuación. Definida en Haskell como ++.
concatenar :: [a] -> [a] -> [a]
concatenar [] [] = []
concatenar (x:xs) [] = (x:xs)
concatenar [] (x:xs) = (x:xs)
concatenar (x:xs) (y:ys) = x : y : (concatenar xs ys)

concatenar' :: [a] -> [a] -> [a]
concatenar' [] ys = ys
concatenar' (x:xs) ys = x : (concatenar' xs ys)

--13. Dada una lista devuelve la lista con los mismos elementos de atrás para adelante. Definida en Haskell como reverse.
reversa :: [a] -> [a]
reversa [] = []
reversa (x:xs) = agregarAlFinal (reversa xs) x
-- reversa (x:xs) = (reversa xs) ++ (x:[])

--14. Dadas dos listas de enteros, devuelve una lista donde el elemento en la posición n es el 
--máximo entre el elemento n de la primera lista y de la segunda lista, teniendo en cuenta que
--las listas no necesariamente tienen la misma longitud.
zipMaximos :: [Int] -> [Int] -> [Int]
zipMaximos xs [] = xs
zipMaximos [] xs = xs
zipMaximos (x:xs)(y:ys) = if (x > y) 
							then x : (zipMaximos xs ys) 
							else y : (zipMaximos xs ys)

--15. Dada una lista devuelve el mínimo
elMinimo :: Ord a => [a] -> a
elMinimo [] = error "no se define el minimo de una lista vacia"
elMinimo [a] = a
elMinimo (x:xs) = min x (elMinimo xs)

-- 2. Recursión sobre números
-- 1. Dado un número n se devuelve la multiplicación de este número y todos sus anteriores hasta
-- llegar a 0. Si n es 0 devuelve 1. La función es parcial si n es negativo.
factorial :: Int -> Int
factorial 0 = 1
factorial n = n * (factorial (n - 1))

-- 2. Dado un número n devuelve una lista cuyos elementos sean los números comprendidos entre
-- n y 1 (incluidos). Si el número es inferior a 1, devuelve la lista vacía.
cuentaRegresiva :: Int -> [Int]
cuentaRegresiva 1 = [1]
cuentaRegresiva n = if n < 0 
						then [] 
						else n : (cuentaRegresiva (n - 1))

-- 3. Dado un número n y un elemento e devuelve una lista en la que el elemento e repite n veces.
repetir :: Int -> a -> [a]
repetir 1 a = (a : [])
repetir n a = a : (repetir (n-1) a)

-- 4. Dados un número n y una lista xs, devuelve una lista con los n primeros elementos de xs.
-- Si la lista es vacía, devuelve una lista vacía.
losPrimeros :: Int -> [a] -> [a]
losPrimeros _ [] = []
losPrimeros 0 _ = []
losPrimeros n (x:xs) = x : (losPrimeros (n-1) xs)

--5. Dados un número n y una lista xs, devuelve una lista sin los primeros n elementos de lista
--recibida. Si n es cero, devuelve la lista completa.
sinLosPrimeros :: Int -> [a] -> [a]
sinLosPrimeros 0 x = x
sinLosPrimeros _ [] = []
sinLosPrimeros n x = (sinLosPrimeros (n-1) (tail x))

--3. Registros
-- 1. Definir el tipo de dato Persona, como un nombre y la edad de la persona. Realizar las siguientes funciones:
data Persona = Persona String Int deriving Show

personaUno = Persona "" 54
personaDos = Persona "" 55
personaTres = Persona "" 15
personaCuatro = Persona "" 36
personaCinco = Persona "" 10
personaSeis = Persona "" 21

personas = personaUno : personaDos : personaTres : personaCuatro : personaCinco : personaSeis : []

edad::Persona -> Int
edad (Persona n e) = e

sumaEdades :: [Persona] -> Int
sumaEdades [] = 0
sumaEdades (x:xs) = (edad x) + (sumaEdades xs)

cantidadPersonas :: [Persona] -> Int
cantidadPersonas [] = 0
cantidadPersonas (x:xs) = 1 + (cantidadPersonas xs)


--Dados una edad y una lista de personas devuelve todas las personas que son mayoresa esa edad.
mayoresA :: Int -> [Persona] -> [Persona]
mayoresA _ [] = []
mayoresA n (p:ps) = if (edad p) > n 
				then [p] ++ (mayoresA n ps)
				else (mayoresA n ps)

-- Dada una lista de personas devuelve el promedio de edad entre esas personas. Precondición: la lista al menos posee una persona.
promedioEdad :: [Persona] -> Int
promedioEdad p = (sumaEdades p) `div` (cantidadPersonas p)

-- Dada una lista de personas devuelve la persona más vieja de la lista. Precondición: la lista al menos posee una persona.
elMasGrande :: Persona -> Persona -> Persona 
elMasGrande p1 p2 = if (edad p1) > (edad p2) then p1 else p2
elMasViejo :: [Persona] -> Persona
elMasViejo [p] = p
elMasViejo (x:xs) = elMasGrande x (elMasViejo xs)

-- 2. Modificaremos la representación de Entreador y Pokemon de la práctica anterior de la siguiente manera:

data TipoDePokemon = Agua | Fuego | Planta
data Pokemon = ConsPokemon TipoDePokemon Int
data Entrenador = ConsEntrenador String [Pokemon]

pokePlanta = ConsPokemon Planta 100
pokeAgua = ConsPokemon Agua 80
pokeFuego  = ConsPokemon Fuego 60
pokeFuego2  = ConsPokemon Fuego 20
entUno = ConsEntrenador "Matias" [pokeAgua,pokePlanta,pokeFuego,pokeFuego,pokeFuego,pokeFuego,pokeFuego]
entDos = ConsEntrenador "Pepe" [pokeAgua,pokeFuego2]


-- Como puede observarse, ahora los entrenadores tienen una cantidad de Pokemon arbitraria.
-- Definir en base a esa representación las siguientes funciones:

pokemones :: Entrenador -> [Pokemon]
pokemones (ConsEntrenador _ p) = p

tipoPoke :: Pokemon -> TipoDePokemon
tipoPoke (ConsPokemon a _) = a

mismoTipo :: TipoDePokemon -> TipoDePokemon -> Bool
mismoTipo Planta Planta = True
mismoTipo Fuego Fuego = True
mismoTipo Agua Agua = True
mismoTipo _ _ = False

--Devuelve la cantidad de pokémon que posee el entrenador.
cantPokemones :: Entrenador -> Int
cantPokemones e = longitud (pokemones e)

-- Devuelve la cantidad de pokémon de determinado tipo que posee el entrenador.
cantPokemonesDe :: TipoDePokemon -> Entrenador -> Int
cantPokemonesDe t e = cantPokemonesDe_desdeLista t (pokemones e) 

cantPokemonesDe_desdeLista :: TipoDePokemon -> [Pokemon] -> Int
cantPokemonesDe_desdeLista _ [] = 0
cantPokemonesDe_desdeLista t (x:xs) = if mismoTipo (tipoPoke x) t 
										then 1 + (cantPokemonesDe_desdeLista t xs)
										else (cantPokemonesDe_desdeLista t xs)

--Dados dos entrenadores, indica la cantidad de Pokemon de cierto tipo del primer entrenador, que le ganarían
--a todos los Pokemon del segundo entrenador.
losQueLeGanan :: TipoDePokemon -> Entrenador -> Entrenador -> Int
losQueLeGanan t e1 e2 = cuantosGananPorTipo t (pokemones e1) (pokemones e2) 

cuantosGananPorTipo :: TipoDePokemon -> [Pokemon] -> [Pokemon] -> Int
cuantosGananPorTipo _ [] _ = 0
cuantosGananPorTipo _ _ x = 0
cuantosGananPorTipo t (x:xs) l = if mismoTipo (tipoPoke x) t 
										then (unoSiLeGanaATodos x l) + (cuantosGananPorTipo t xs l)
										else (cuantosGananPorTipo t xs l)

unoSiLeGanaATodos :: Pokemon -> [Pokemon] -> Int
unoSiLeGanaATodos p l = if leGanaATodos p l then 1 else 0

leGanaATodos:: Pokemon -> [Pokemon] -> Bool
leGanaATodos p [] = True
leGanaATodos p (x:xs) = (leGana p x) && (leGanaATodos p xs)

leGana :: Pokemon -> Pokemon -> Bool
leGana p1 p2 = esMasFuerte (tipoPoke p1) (tipoPoke p2)

esMasFuerte :: TipoDePokemon -> TipoDePokemon -> Bool
esMasFuerte Agua Fuego = True
esMasFuerte Fuego Planta = True
esMasFuerte Planta Agua = True 
esMasFuerte _ _ = False

-- Dado un entrenador, devuelve True si posee al menos un pokémon de cada tipo posible.
todosLosTipo = [Fuego, Agua, Planta]
esMaestroPokemon :: Entrenador -> Bool
esMaestroPokemon e = tieneTodosLosTipos e todosLosTipo 

tieneTodosLosTipos :: Entrenador -> [TipoDePokemon] -> Bool
tieneTodosLosTipos e [] = True
tieneTodosLosTipos e (x:xs) = (tieneDelTipo (pokemones e) x) && (tieneTodosLosTipos e xs)

tieneDelTipo :: [Pokemon] -> TipoDePokemon -> Bool
tieneDelTipo [] _ = False
tieneDelTipo (x:xs) t = (mismoTipo t (tipoPoke x) ) || (tieneDelTipo xs t)

--3. El tipo de dato Rol representa los roles (desarollo o management) de empleados IT dentro
--de una empresa de software, junto al proyecto en el que se encuentran. Así, una empresa es
--una lista de personas con diferente rol. La definición es la siguiente:
data Seniority = Junior | SemiSenior | Senior deriving Show
data Proyecto = ConsProyecto String deriving Show
data Rol = Developer Seniority Proyecto | Management Seniority Proyecto deriving Show
data Empresa = ConsEmpresa [Rol] deriving Show

proyUno = ConsProyecto "ProyUno"
proyDos = ConsProyecto "ProyDos"
proyTres = ConsProyecto "proyTres"

rolUno = Developer Junior proyUno
rolDos = Developer SemiSenior proyUno
rolTres = Developer Senior proyUno
rolCuatro = Developer Senior proyDos
rolCinco = Developer Senior proyTres
rolSeis = Management Senior proyUno
rolSiete = Management Senior proyDos
rolOcho = Management Senior proyTres

empresa = ConsEmpresa [rolUno,rolDos,rolTres,rolCuatro,rolCinco,rolSeis,rolSiete,rolOcho]

--Definir las siguientes funciones sobre el tipo Empresa:
--Dada una empresa denota la lista de proyectos en los que trabaja, sin elementos repetidos.
rolesDeLaEmpresa :: Empresa -> [Rol]
rolesDeLaEmpresa (ConsEmpresa x) = x

nombreProy :: Proyecto -> String
nombreProy (ConsProyecto n) = n

proyectos :: Empresa -> [Proyecto]
proyectos x = sinRepetidos (proyectosDeLaEmpresa (rolesDeLaEmpresa x)) 

sinRepetidos :: [Proyecto] -> [Proyecto]
sinRepetidos [] = []
sinRepetidos (x:xs) = if exiteProyectoEn x xs
						then sinRepetidos xs
						else x : sinRepetidos xs

exiteProyectoEn :: Proyecto -> [Proyecto] -> Bool
exiteProyectoEn e [] = False
exiteProyectoEn e (x:xs) = (nombreProy x) == (nombreProy e) || (exiteProyectoEn e xs)


proyectosDeLaEmpresa :: [Rol] -> [Proyecto]
proyectosDeLaEmpresa [] = []
proyectosDeLaEmpresa (x:xs) = (proyectoDelRol x) : (proyectosDeLaEmpresa xs)

proyectoDelRol :: Rol -> Proyecto
proyectoDelRol (Developer sen proy) = proy
proyectoDelRol (Management sen proy) = proy


--Dada una empresa indica la cantidad de desarrolladores senior que posee.
losDevSenior :: Empresa -> Int
losDevSenior x = cantSenior (rolesDeLaEmpresa x)

cantSenior :: [Rol] -> Int
cantSenior [] = 0
cantSenior (x:xs) = (unoSiEsSenior (seniority x))+(cantSenior xs)

seniority::Rol->Seniority
seniority (Developer sen proy) = sen
seniority (Management sen proy) = sen

unoSiEsSenior::Seniority->Int
unoSiEsSenior Senior = 1
unoSiEsSenior _ = 0

-- Indica la cantidad de empleados que trabajan en alguno de los proyectos dados.
cantQueTrabajanEn :: [Proyecto] -> Empresa -> Int
cantQueTrabajanEn [] _ = 0
cantQueTrabajanEn (x:xs) e = (empleadosDelProy x (rolesDeLaEmpresa e)) + (cantQueTrabajanEn xs e)

empleadosDelProy :: Proyecto -> [Rol] -> Int
empleadosDelProy _ [] = 0
empleadosDelProy p (x:xs) = if esDelProtecto x p 
							 then 1 + (empleadosDelProy p xs)
							 else (empleadosDelProy p xs)

esDelProtecto :: Rol -> Proyecto -> Bool
esDelProtecto r p = (nombreProy (proyectoDelRol r)) == (nombreProy p)

--Devuelve una lista de pares que representa a los proyectos (sin repetir) junto con su cantidad de personas involucradas.
asignadosPorProyecto :: Empresa -> [(Proyecto, Int)]
asignadosPorProyecto e = (cantidadDeEmpleadosPorProyecto (proyectos e) e)

cantidadDeEmpleadosPorProyecto :: [Proyecto] -> Empresa -> [(Proyecto, Int)]
cantidadDeEmpleadosPorProyecto [] _ = []
cantidadDeEmpleadosPorProyecto (x:xs) e = [(x,(cantQueTrabajanEn [x] e))] ++ (cantidadDeEmpleadosPorProyecto xs e)