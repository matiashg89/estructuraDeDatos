import Map1

-- Propósito: obtiene los valores asociados a cada clave del map.
valuesM :: Eq k => Map k v -> [Maybe v]
valuesM map = obtenerValores (keys map) map

obtenerValores :: Eq k => [k] -> Map k v -> [Maybe v]
obtenerValores [] _ = [Nothing]
obtenerValores (x : xs) map = (lookupM x map) : (obtenerValores xs map)

-- Propósito: indica si en el map se encuenrtan todas las claves dadas.
todasAsociadas :: Eq k => [k] -> Map k v -> Bool
todasAsociadas (x:xs) map = ((lookupM x map) != Nothing) && (todasAsociadas xs map)

-- Propósito: convierte una lista de pares clave valor en un map.
listToMap :: Eq k => [(k, v)] -> Map k v 
listToMap (x:xs) = assocM (fst x) (snd x) (listToMap xs)

-- Propósito: convierte un map en una lista de pares clave valor.
mapToList :: Eq k => Map k v -> [(k, v)]
mapToList map = convertirALista (keys map) (obtenerValores map)

convertirALista :: Eq k => [k] -> [v] -> [(k,v)]
convertirALista [] [] = []
convertirALista (k:ks)(v:vs) = [(k,v)] ++ (convertirALista ks vs)

-- Propósito: dada una lista de pares clave valor, agrupa los valores de los pares que compartan
-- la misma clave.
-- agruparEq :: Eq k => [(k, v)] -> Map k [v]
-- gruparEq [] = emptyM
-- gruparEq (x:xs) = if lookupM (fst x) != Nothing
-- 					then 
-- 					else assocM (fst x) (snd x) (agruparEq xs)

-- [
-- (1,5),
-- (2,7),
-- (3,32),
-- (1,34),
-- (3,0),
-- (1,42),
-- (5,56)]

-- Propósito: dada una lista de claves de tipo k y un mapa que va de k a int, le suma uno a
-- cada número asociado con dichas claves.
-- incrementar :: Eq k => [k] -> Map k Int -> Map k Int
-- incrementar [] map = map
-- incrementar (x:xs) map = 
-- Propósito: dado dos maps se agregan las claves y valores del primer map en el segundo. Si
-- una clave del primero existe en el segundo, es reemplazada por la del primero.
-- mergeMaps:: Eq k => Map k v -> Map k v -> Map k v