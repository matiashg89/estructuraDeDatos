-- 1 - Números enteros
 -- 1.a
sucesor :: Int -> Int
sucesor x = x + 1

 -- 1.b
sumar :: Int -> Int -> Int
sumar x y = x + y

 -- 1.c
divisionYResto :: Int -> Int -> (Int, Int)
divisionYResto x y = (div x y, mod x y)

--otra forma
--divisionYResto :: Int -> Int -> (Int, Int)
--divisionYResto _ 0 = error "no me mandes un cero"
--divisionYResto x y = (div a b, mod a b)

 -- 1.d
maxDelPar :: (Int,Int) -> Int
maxDelPar (a, b) = if fst (a, b) > snd (a, b) then a else b

-- 2
--1) sucesor 9
--2) suma 9 1
--3) maxDelPar (10,1)
--4) suma 8 2

-- 2 - Tipos enumerativos
-- 1. Definir el tipo de dato Dir, con las alternativas Norte, Sur, Este y Oeste. Luego implementar
-- las siguientes funciones
data Dir = Norte | Sur | Este | Oeste deriving Show

-- a) Dada una dirección devuelve su opuesta
opuesto :: Dir -> Dir
opuesto Norte = Sur
opuesto Sur = Norte
opuesto Oeste = Este
opuesto Este = Oeste

-- b) Dadas dos direcciones, indica si son la misma. Nota: utilizar pattern matching y no ==
iguales :: Dir -> Dir -> Bool
iguales Norte Norte = True
iguales Sur Sur = True
iguales Oeste Oeste = True
iguales Este Este = True
iguales _ _ = False

--c) Dada una dirección devuelve su siguiente, en sentido horario, y suponiendo que no existe
--la siguiente dirección a Oeste. ¿Posee una precondición esta función? ¿Es una función
--total o parcial? ¿Por qué?
siguiente :: Dir -> Dir
siguiente Norte = Este
siguiente Este = Sur
siguiente Sur = Oeste
siguiente Oeste = Norte

-- 2. Definir el tipo de dato DiaDeSemana, con las alternativas Lunes, Martes, Miércoles, Jueves, 
-- Viernes, Sabado y Domingo. Supongamos que el primer día de la semana es lunes, y el último
-- es domingo. Luego implementar las siguientes funciones
data DiaDeSemana = Lunes | Martes | Miercoles | Jueves | Viernes | Sabado | Domingo deriving Show

--a) Devuelve un par donde la primera componente es el primer día de la semana, y la
--segunda componente es el último día de la semana.
primeroYUltimoDia :: (DiaDeSemana, DiaDeSemana)
primeroYUltimoDia = (Domingo, Sabado)


-- b) Dado un dia de la semana indica si comienza con la letra M.
empiezaConM :: DiaDeSemana -> Bool
empiezaConM Lunes = False 
empiezaConM Martes = True
empiezaConM Miercoles = True
empiezaConM Jueves = False
empiezaConM Viernes = False
empiezaConM Sabado = False
empiezaConM Domingo = False

-- c) Dado dos dias de semana, indica si el primero viene después que el segundo.
indiceDiaSemana :: DiaDeSemana -> Int
indiceDiaSemana Domingo = 0
indiceDiaSemana Lunes = 1 
indiceDiaSemana Martes = 2
indiceDiaSemana Miercoles = 3
indiceDiaSemana Jueves = 4
indiceDiaSemana Viernes = 5
indiceDiaSemana Sabado = 6

vieneDespues :: DiaDeSemana -> DiaDeSemana -> Bool
vieneDespues a b = indiceDiaSemana a > indiceDiaSemana b

-- d) Dado un dia de la semana indica si no es ni el primer ni el ultimo dia.
estaEnElMedio :: DiaDeSemana -> Bool
estaEnElMedio a = indiceDiaSemana a > 0 && indiceDiaSemana a < 6

-- 3
-- a) Dado un booleano, si es T rue devuelve F alse, y si es F alse devuelve T rue. En Haskell ya está definida como not.
negar :: Bool -> Bool
negar True = False
negar False = True

-- b) Dados dos booleanos, si el primero es True y el segundo es F alse, devuelve F alse, sino devuelve T rue.
-- Nota: no viene implementada en Haskell.
implica :: Bool -> Bool -> Bool
implica a b = a && negar b
implica _ _ = True

-- c) Dados dos booleanos si ambos son True devuelve True, sino devuelve F alse.
-- En Haskell ya está definida como &&.
and :: Bool -> Bool -> Bool
and True True = True
and _ _ = False

-- d) Dados dos booleanos si alguno de ellos es T rue devuelve T rue, sino devuelve F alse.
-- En Haskell ya está definida como ||.
or :: Bool -> Bool -> Bool
or False False = False
or _ _ = True

-- 3. Registros
-- 1. Definir el tipo de dato Persona, como un nombre y la edad de la persona. Realizar las siguientes funciones:
data Persona = Persona String Int deriving Show

p1 = Persona "matias" 31
p2 = Persona "raul" 60
--Devuelve el nombre de una persona
nombre :: Persona -> String
nombre (Persona n _ ) = n 

-- Devuelve la edad de una persona
edad :: Persona -> Int
edad (Persona _ e) = e

-- Aumenta en uno la edad de la persona.
crecer :: Persona -> Persona
crecer x = (Persona (nombre x) ((edad x +) 1))

-- Dados un nombre y una persona, devuelve una persona con la edad de la persona y el nuevo nombre.
cambioDeNombre :: String -> Persona -> Persona
cambioDeNombre nombre p = (Persona nombre (edad p))

--Dadas dos personas indica si la primera es mayor que la segunda.
esMayorQueLaOtra :: Persona -> Persona -> Bool
esMayorQueLaOtra pUno pDos = edad pUno > edad pDos

--Dadas dos personas devuelve a la persona que sea mayor.
laQueEsMayor :: Persona -> Persona -> Persona
laQueEsMayor pUno pDos = if edad pUno > edad pDos then pUno else pDos

-- 2. Definir los tipos de datos Pokemon, como un TipoDePokemon (agua, fuego o planta) y un porcentaje de energía; y Entrenador, como un 
--nombre y dos pokemones. Luego definir las siguientes funciones:


data TipoDePokemon = Agua | Fuego | Planta deriving Show
data Pokemon = Pokemon TipoDePokemon Int deriving Show
pokePlanta = Pokemon Planta 100
pokeAgua = Pokemon Agua 80
pokeFuego  = Pokemon Fuego 60
pokeFuego2  = Pokemon Fuego 20
data Entrenador = Entrenador String Pokemon Pokemon
entUno = Entrenador "Matias" pokePlanta pokeFuego
entDos = Entrenador "Pepe" pokeAgua pokeFuego2
--Dados dos pokémon indica si el primero, en base al tipo, es superior al segundo. Agua
--supera a fuego, fuego a planta y planta a agua. Y cualquier otro caso es falso.
debilidad :: TipoDePokemon -> TipoDePokemon
debilidad Fuego = Agua
debilidad Agua = Planta 
debilidad Planta = Fuego

tipoPoke :: Pokemon -> TipoDePokemon
tipoPoke (Pokemon a _) = a

mismoTipo :: TipoDePokemon -> TipoDePokemon -> Bool
mismoTipo Planta Planta = True
mismoTipo Fuego Fuego = True
mismoTipo Agua Agua = True
mismoTipo _ _ = False

superaA :: Pokemon -> Pokemon -> Bool
superaA pokeUno pokeDos = mismoTipo (debilidad (tipoPoke pokeDos)) (tipoPoke pokeUno)

cantidadIguales :: TipoDePokemon -> TipoDePokemon -> Int
cantidadIguales Planta Planta = 1
cantidadIguales Fuego Fuego = 1
cantidadIguales Agua Agua = 1
cantidadIguales _ _ = 0

-- Devuelve la cantidad de pokémon de determinado tipo que posee el entrenador.
cantidadDePokemonesDe :: TipoDePokemon -> Entrenador -> Int
cantidadDePokemonesDe tipo (Entrenador _ pokeUno pokeDos) = (cantidadIguales (tipoPoke pokeUno) tipo) + 
															(cantidadIguales (tipoPoke pokeDos) tipo)
-- Dado un par de entrenadores, devuelve a sus pokemones en una lista.
juntarPokemones :: (Entrenador, Entrenador) -> [Pokemon]
juntarPokemones ((Entrenador _ pokeUno pokeDos), (Entrenador _ pokeTres pokeCuatro)) = pokeUno : pokeDos : pokeTres : pokeCuatro : []


-- 1. Defina las siguientes funciones polimórficas:
--a) Dado un elemento de algún tipo devuelve ese mismo elemento.
loMismo :: a -> a
loMismo x = x
--b) Dado un elemento de algún tipo devuelve el número 7.
siempreSiete :: a -> Int
siempreSiete _ = 7
--c) Dadas una tupla, invierte sus componentes.
--¿Por qué existen dos variables de tipo diferentes?
-- Porque la funcion soporta varios tipos, independientemente el valor que se le pase, siempre los va a invertir
swap :: (a,b) -> (b, a)
swap (x,y) = (y, x)

-- 2. Responda la siguiente pregunta: ¿Por qué estas funciones son polimórficas?
-- No importa el tipo que se de, siempre da el mismo resultado

-- 5. Pattern matching sobre listas
--Defina las siguientes funciones polimórficas utilizando pattern matching sobre listas (no utilizar las funciones que ya vienen con Haskell):

--2. Dada una lista de elementos, si es vacía devuelve T rue, sino devuelve F alse. Definida en Haskell como null.
estaVacia :: [a] -> Bool
estaVacia [] = True
estaVacia _ = False

--3. Dada una lista devuelve su primer elemento. Definida en Haskell como head.
--Nota: tener en cuenta que el constructor de listas es :
elPrimero :: [a] -> a
elPrimero [] = error "No tiene ningun elemento"
elPrimero (a : xs) = a

--4. Dada una lista devuelve esa lista menos el primer elemento. Definida en Haskell como tail.
--Nota: tener en cuenta que el constructor de listas es :
sinElPrimero :: [a] -> [a]
sinElPrimero [] = error "No tiene ningun elemento"
sinElPrimero (a : xs) = xs

--5. Dada una lista devuelve un par, donde la primera componente es el primer elemento de la lista, y la segunda componente es esa lista pero sin el primero.
--Nota: tener en cuenta que el constructor de listas es :
splitHead :: [a] -> (a, [a])
splitHead a = (elPrimero a, sinElPrimero a)