module Stack1(
	Stack,
	emptyS,
	isEmptyS
	push,
	pop,
	lenS
) where

data (Eq a) => Stack a = S [a] deriving Show


-- Crea una pila vacía.
emptyS :: Stack a
emptyS = S []

-- Dada una pila indica si está vacía.
isEmptyS :: Stack a -> Bool
isEmptyS (S lista) = length lista == 0

-- Dados un elemento y una pila, agrega el elemento a la pila.
push :: a -> Stack a -> Stack a
push a (S lista) = [a] ++ lista

-- Dada un pila devuelve el elemento del tope de la pila.
top :: Stack a -> a
top (S lista) = head lista

-- Dada una pila devuelve la pila sin el primer elemento.
pop :: Stack a -> Stack a
pop (S lista) = tail lista

-- Dada una pila denota la cantidad de elementos del Stack
-- Costo: constante.
lenS :: Stack a -> Int
lenS (S lista) = length lista