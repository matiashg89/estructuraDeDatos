--1. Tipos recursivos simples
--1.1. Celdas con bolitas

data Color = Azul | Rojo deriving Show
data Celda = Bolita Color Celda | CeldaVacia deriving Show

celdas = Bolita Rojo (Bolita Azul (Bolita Rojo (Bolita Azul CeldaVacia)))

--Dados un color y una celda, indica la cantidad de bolitas de ese color. Nota: pensar si ya existe una operación sobre listas que ayude a resolver el problema.
nroBolitas :: Color -> Celda -> Int
nroBolitas _ CeldaVacia = 0
nroBolitas color (Bolita col cel) = if esMismoColor color col 
										then 1 + (nroBolitas color cel )
										else (nroBolitas color cel )

esMismoColor :: Color -> Color -> Bool
esMismoColor Azul Azul = True
esMismoColor Rojo Rojo = True
esMismoColor _ _ = False

-- Dado un color y una celda, agrega una bolita de dicho color a la celda.
poner :: Color -> Celda -> Celda
poner nuevoColor CeldaVacia = Bolita nuevoColor CeldaVacia
poner nuevoColor (Bolita color celda) = (Bolita color (poner nuevoColor celda))

-- Dado un color y una celda, quita una bolita de dicho color de la celda. Nota: a diferencia de Gobstones, esta función es total.
-- VER !!!!!!!!!!!!!!!!!!!
sacar :: Color -> Celda -> Celda
sacar _ CeldaVacia = CeldaVacia
sacar color (Bolita col celda) = if esMismoColor color col 
									then celda
									else Bolita col (sacar color celda)

--sacar :: Color -> Celda -> Celda
--sacar _ CeldaVacia = CeldaVacia
--sacar Azul (Bolita Azul celda) = celda
--sacar Rojo (Bolita Rojo celda) = celda
--sacar c (Bolita color celda) = Bolita color (sacar c celda)

--Dado un número n, un color c, y una celda, agrega n bolitas de color c a la celda.
ponerN :: Int -> Color -> Celda -> Celda
ponerN 0 color c = c
ponerN i color c = ponerN (i-1) color (poner color c)


--1.2. Camino hacia el tesoro
--Tenemos los siguientes tipos de datos
data Objeto = Cacharro | Tesoro deriving Show
data Camino = Fin | Cofre [Objeto] Camino | Nada Camino deriving Show

camino1 = Cofre [Cacharro, Cacharro] (Nada (Cofre [Cacharro,Tesoro] Fin))
camino2 = Cofre [Cacharro] (Nada (Cofre [Cacharro, Cacharro] Fin))
camino3 = Nada (Nada (Nada (Cofre [Tesoro] Fin)))
camino4 = Cofre [Tesoro] (Nada Fin)

-- Indica si hay un cofre con un tesoro en el camino.
hayTesoro :: Camino -> Bool
hayTesoro Fin = False
hayTesoro (Cofre objetos camino) = hayTesoro' objetos || (hayTesoro camino) 
hayTesoro (Nada camino) = (hayTesoro camino)

hayTesoro' :: [Objeto] -> Bool
hayTesoro' [] = False
hayTesoro' (x:xs) = (esMismoObjeto x Tesoro) || (hayTesoro' xs)

esMismoObjeto :: Objeto -> Objeto -> Bool
esMismoObjeto Tesoro Tesoro = True
esMismoObjeto Cacharro Cacharro = True
esMismoObjeto _ _ = False

--Indica la cantidad de pasos que hay que recorrer hasta llegar al primer cofre con un tesoro.
--Si un cofre con un tesoro está al principio del camino, la cantidad de pasos a recorrer es 0.
--Precondición: tiene que haber al menos un tesoro.
pasosHastaTesoro :: Camino -> Int
pasosHastaTesoro Fin = 0
pasosHastaTesoro (Nada restoCamino) = 1 + (pasosHastaTesoro restoCamino)
pasosHastaTesoro (Cofre objetos restoCamino) = if hayTesoroEnLaLista objetos
											   	then (pasosHastaTesoro Fin)
											   	else 1 + (pasosHastaTesoro restoCamino)

hayTesoroEnLaLista :: [Objeto] -> Bool
hayTesoroEnLaLista [] = False
hayTesoroEnLaLista (x:xs) = (esMismoObjeto x Tesoro) || (hayTesoroEnLaLista xs)

--Indica si hay un tesoro en una cierta cantidad exacta de pasos. Por ejemplo, si el número de pasos es 5, indica si hay un tesoro en 5 pasos.
hayTesoroEn :: Int -> Camino -> Bool
hayTesoroEn 0 camino = (esCofre camino) && (hayTeosoroEnElCofre camino)
hayTesoroEn n Fin = False
hayTesoroEn n (Nada camino) = (hayTesoroEn (n-1) camino) 
hayTesoroEn n (Cofre lista camino) = (hayTesoroEn (n-1) camino )

esCofre :: Camino -> Bool
esCofre (Cofre x s) = True
esCofre _ = False

hayTeosoroEnElCofre :: Camino -> Bool
hayTeosoroEnElCofre (Cofre lista camino) = (hayTesoroEnLaLista lista)
hayTeosoroEnElCofre _ = False

--Indica si hay al menos “n” tesoros en el camino.
alMenosNTesoros :: Int -> Camino -> Bool
alMenosNTesoros n camino = cantidadTesorosEnCamino camino >= n

cantidadTesorosEnCamino :: Camino -> Int
cantidadTesorosEnCamino Fin = 0
cantidadTesorosEnCamino (Nada camino) = (cantidadTesorosEnCamino camino)
cantidadTesorosEnCamino (Cofre lista camino) = (cantidadDeTesorosEnLista lista) + (cantidadTesorosEnCamino camino)

cantidadDeTesorosEnLista :: [Objeto] -> Int
cantidadDeTesorosEnLista [] = 0
cantidadDeTesorosEnLista (x:xs) = if esMismoObjeto x Tesoro
									then 1 + (cantidadDeTesorosEnLista xs)
									else (cantidadDeTesorosEnLista xs)

--Dado un rango de pasos, indica la cantidad de tesoros que hay en ese rango. Por ejemplo, si
--el rango es 3 y 5, indica la cantidad de tesoros que hay entre hacer 3 pasos y hacer 5. Están
--incluidos tanto 3 como 5 en el resultado.
-- VER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
cantTesorosEntre :: Int -> Int -> Camino -> Int
cantTesorosEntre 0 0 camino = cantidadTesorosEnCaminoActual camino
cantTesorosEntre 0 fin camino = (cantidadTesorosEnCaminoActual camino) +  (cantTesorosEntre 0 (fin-1) camino)
cantTesorosEntre inicio fin camino = (cantTesorosEntre (inicio - 1) fin camino)

cantidadTesorosEnCaminoActual :: Camino -> Int
cantidadTesorosEnCaminoActual Fin = 0
cantidadTesorosEnCaminoActual (Nada camino) = 0
cantidadTesorosEnCaminoActual (Cofre lista camino) = cantidadDeTesorosEnLista lista

-- cantTesorosEntre :: Int -> Int -> Camino -> Int
-- cantTesorosEntre 0 m Fin = False
-- cantTesorosEntre n m Fin = False
-- cantTesorosEntre 0 m (Cofre objs cam) = 

-- cantTesorosEntre n m (Cofre objs cam) = 

-- 	..
-- cantTesorosEntre 0 m (Nada cam) = 
-- cantTesorosEntre m m (Nada cam) = 

-- 	.. 

--2. Tipos arbóreos
--2.1. Árboles binarios
--Dada esta definición para árboles binarios
data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving Show


arbol1 :: Tree Int
arbol1 = NodeT 10 
            (NodeT 20
                (NodeT 30
                    EmptyT
                    EmptyT)
                EmptyT)
            EmptyT

--defina las siguientes funciones utilizando recursión estructural según corresponda:
--1. Dado un árbol binario de enteros devuelve la suma entre sus elementos.
sumarT :: Tree Int -> Int
sumarT EmptyT = 0 
sumarT (NodeT i tl tr) = i + (sumarT tl) + (sumarT tr) 

--2. Dado un árbol binario devuelve su cantidad de elementos, es decir, el tamaño del árbol (size en inglés).
sizeT :: Tree a -> Int
sizeT EmptyT = 0
sizeT (NodeT a ramaUno ramaDos) = 1 + (sizeT ramaUno) + (sizeT ramaDos)

--3. Dado un árbol de enteros devuelve un árbol con el doble de cada número.
mapDobleT :: Tree Int -> Tree Int
mapDobleT EmptyT = EmptyT 
mapDobleT (NodeT a bl br) = (NodeT (a*2) (mapDobleT(bl)) (mapDobleT(br)))

--4. Dados un elemento y un árbol binario devuelve True si existe un elemento igual a ese en el árbol.
perteneceT :: Eq a => a -> Tree a -> Bool
perteneceT n EmptyT = False
perteneceT n (NodeT a bl br) = a == n || (perteneceT n bl) || (perteneceT n br)

--5. Dados un elemento e y un árbol binario devuelve la cantidad de elementos del árbol que son iguales a e.
aparicionesT :: Eq a => a -> Tree a -> Int
aparicionesT n EmptyT = 0
aparicionesT n (NodeT a bl br) = if a == n
									then 1 + (aparicionesT n bl) + (aparicionesT n br)
									else (aparicionesT n bl) + (aparicionesT n br)

--6. Dado un árbol devuelve los elementos que se encuentran en sus hojas.
-- VER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
leaves :: Tree a -> [a]
leaves EmptyT = []
leaves (NodeT a EmptyT EmptyT) = [a]
leaves (NodeT a bl br) =  (leaves bl) ++ (leaves br)

--7. Dado un árbol devuelve su altura.
--Nota: la altura de un árbol (height en inglés), también llamada profundidad, es la cantidad
--de niveles del árbol. La altura de un árbol vacío es cero y la de una hoja también.
heightT :: Tree a -> Int
heightT EmptyT = 0
heightT (NodeT a bl br) = 1 + max (heightT bl) (heightT br)

--8. Dado un árbol devuelve el árbol resultante de intercambiar el hijo izquierdo con el derecho, en cada nodo del árbol.
mirrorT :: Tree a -> Tree a
mirrorT EmptyT = EmptyT
mirrorT (NodeT a bl br) = NodeT a (mirrorT bl) (mirrorT br)

-- 9. Dado un árbol devuelve una lista que representa el resultado de recorrerlo en modo in-order.
-- Nota: En el modo in-order primero se procesan los elementos del hijo izquierdo, luego la raiz
-- y luego los elementos del hijo derecho.
toList :: Tree a -> [a]
toList EmptyT = []
toList (NodeT a bl br) = (toList bl) ++ [a] ++ (toList br)

-- 10. Dados un número n y un árbol devuelve una lista con los nodos de nivel n. El nivel de un
-- nodo es la distancia que hay de la raíz hasta él. La distancia de la raiz a sí misma es 0, y la
-- distancia de la raiz a uno de sus hijos es 1.
-- Nota: El primer nivel de un árbol (su raíz) es 0
levelN :: Int -> Tree a -> [a]
levelN num EmptyT = []
levelN 0 (NodeT n ti td) = [n]
levelN num (NodeT n ti td) = (levelN (num-1) ti) ++ (levelN (num-1) td)


-- 11. Dado un árbol devuelve una lista de listas en la que cada elemento representa un nivel de dicho árbol.
listPerLevel :: Tree a -> [[a]]
listPerLevel EmptyT = []
listPerLevel (NodeT a bl br) = [a] : (unirPorNivel (listPerLevel bl) (listPerLevel br))

unirPorNivel :: [[a]] -> [[a]] -> [[a]]
unirPorNivel [] x = x
unirPorNivel x [] = x
unirPorNivel (x:xs)(y:ys) = (x ++ y) : (unirPorNivel xs ys)

-- 12. Devuelve los elementos de la rama más larga del árbol
ramaMasLarga :: Tree a -> [a]
ramaMasLarga EmptyT = []
ramaMasLarga (NodeT x t1 t2) = if heightT t1 >= heightT t2
								then x : ramaMasLarga t1 
								else x : ramaMasLarga t2

-- 13. Dado un árbol devuelve todos los caminos, es decir, los caminos desde la raiz hasta las hojas.
-- todosLosCaminos :: Tree a -> [[a]]
-- todosLosCaminos EmptyT = []
-- todosLosCaminos (NodeT n ti td) =
-- 							agregarA n (todosLosCaminos ti) ++ agregarA n (todosLosCaminos td)

-- agregarA :: a -> [[a]] -> [[a]]
-- agregarA n [] = [[n]]
-- agregarA n (x:[]) = (n : x) : [] 
-- agregarA n (x:xs) = (n : x) : (agregarA n xs)

todosLosCaminos :: Tree a -> [[a]]
todosLosCaminos EmptyT = []
todosLosCaminos (NodeT x EmptyT EmptyT) = [[x]]
todosLosCaminos (NodeT x ti td) = agregarA x (todosLosCaminos ti) ++ agregarA x (todosLosCaminos td)

-- Propósito: agrega n a cada lista
agregarA :: a -> [[a]] -> [[a]]
agregarA n []  = []
agregarA n (x:xs) =
	(n : x) : agregarA n xs

-- 2.2. Expresiones Lógicas
-- El tipo algebraico ExpL modela expresiones lógicas de la siguiente manera:
data ExpL = Valor Bool
		| And ExpL ExpL
		| Or ExpL ExpL
		| Not ExpL deriving Show

exp1 = Or (
		Not(
			And(Valor True) 
				(Valor False)
			))(Valor False)

-- 1. Dada una expresión lógica devuelve el resultado booleano de evaluarla. Precondición: todas
-- las variables están asignadas.
evalL :: ExpL -> Bool
evalL (Valor b) = b
evalL (And expA expB) = (evalL expA) && (evalL expB)
evalL (Or expA expB) = (evalL expA) || (evalL expB)
evalL (Not expA) = not (evalL expA)

-- 2. Dada una expresión lógica, la simplifica según los siguientes criterios (descriptos utilizando
-- notación matemática convencional):
-- a) False || x = x || False = x
-- b) True || x = x || True = True
-- c) True && x = x && True = x
-- d) False && x = x && False = False


-- a) False || x = x
-- b) True || x = True
-- c) True && x = x
-- d) False && x = False

simplificarL :: ExpL -> ExpL
simplificarL (Or a b) = if (evalL a)
							then (Valor True) 
							else (simplificarL b)
simplificarL (And a b) = if (evalL a) 
							then (Valor False) 
							else (simplificarL b)
simplificarL (Not a) = (simplificarL a)
simplificarL x = x



-- /*-------------------------------------*/
-- /*---- RESOLVER -----------------------*/
-- /*-------------------------------------*/
-- cantTesorosEntre