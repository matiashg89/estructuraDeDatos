-- 1. Cálculo de costos
-- Especificar el costo operacional de las siguientes funciones:
head' :: [a] -> a
head' (x:xs) = x
-- Constante O(1)

sumar :: Int -> Int
sumar x = x + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1
-- Constante O(1)

factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n-1)
-- Lineal O(n)

longitud :: [a] -> Int
longitud [] = 0
longitud (x:xs) = 1 + longitud xs
-- Lineal O(n)

factoriales :: [Int] -> [Int]
factoriales [] = []
factoriales (x:xs) = factorial x : factoriales xs
-- Cuadratico O(n2) -> Por cada operacione se realiza una operacion lineal (factorial)

pertenece :: Eq a => a -> [a] -> Bool
pertenece n [] = False
pertenece n (x:xs) = n == x || pertenece n xs
-- Lineal O(n) -> porque se hace recursion sobre la lista

sinRepetidos :: Eq a => [a] -> [a]
sinRepetidos [] = []
sinRepetidos (x:xs) =
	if pertenece x xs
		then sinRepetidos xs
		else x : sinRepetidos xs
-- Cuadratico O(n2)

-- equivalente a (++)
append :: [a] -> [a] -> [a]
append [] ys = ys
append (x:xs) ys = x : append xs ys
-- Lineal O(n)

concatenar :: [String] -> String
concatenar [] = []
concatenar (x:xs) = x ++ concatenar xs
-- Cuadratico O(n2)

takeN :: Int -> [a] -> [a]
takeN 0 xs = []
takeN n [] = []
takeN n (x:xs) = x : takeN (n-1) xs
-- Lineal O(n)

dropN :: Int -> [a] -> [a]
dropN 0 xs = xs
dropN n [] = []
dropN n (x:xs) = dropN (n-1) xs
-- Lineal O(n)

partir :: Int -> [a] -> ([a], [a])
partir n xs = (takeN n xs, dropN n xs)
-- Lineal O(n) donde n es la cantidad de elementos a tomar y descartar

minimo :: Ord a => [a] -> a
minimo [x] = x
minimo (x:xs) = min x (minimo xs)
-- Cuadratico O(n2)

sacar :: Eq a => a -> [a] -> [a]
sacar n [] = []
sacar n (x:xs) =
	if n == x
		then xs
		else x : sacar n xs
-- Cuadratico O(n2)

ordenar :: Ord a => [a] -> [a]
ordenar [] = []
orderar xs =
	let m = minimo xs
		in m : ordenar (sacar m xs)
-- Cubico O(n3)
