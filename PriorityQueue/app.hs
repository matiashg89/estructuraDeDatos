import PriorityQueue2

ejemploPQ :: (Ord a) => PriorityQueue [a]
ejemploPQ = emptyPQ

-- Dada una lista la ordena de menor a mayor utilizando una PriorityQueue como estructura auxiliar.
heapSort :: Ord a => [a] -> [a]
heapSort lista = pqToList (listToPq lista)

pqToList :: Ord a => PriorityQueue a -> [a]
pqToList pq = if isEmptyPQ pq
				then []
				else (findMinPQ pq) : pqToList(deleteMinPQ pq)

listToPq :: Ord a => [a] -> PriorityQueue a
listToPq [] = emptyPQ
listToPq (x:xs) = insertPQ x (listToPq xs)