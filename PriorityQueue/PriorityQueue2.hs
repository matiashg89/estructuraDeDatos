module PriorityQueue2(
	PriorityQueue,emptyPQ,isEmptyPQ,findMinPQ,deleteMinPQ,insertPQ
) where

data (Ord a) => PriorityQueue a = PQ [a] deriving Show


-- Propósito: devuelve una priority queue vacía.
emptyPQ :: (Ord a) => PriorityQueue a
emptyPQ = PQ []

-- Propósito: indica si la priority queue está vacía.
isEmptyPQ :: (Ord a) => PriorityQueue a -> Bool
isEmptyPQ (PQ lista) = null lista

-- Propósito: inserta un elemento en la priority queue. Lo inserta ordenado
insertPQ :: Ord a => a -> PriorityQueue a -> PriorityQueue a
insertPQ elemento (PQ lista) = PQ (insertarElementoOrdenado elemento lista)

insertarElementoOrdenado :: Ord a => a -> [a] -> [a]
insertarElementoOrdenado a [] = [a]
insertarElementoOrdenado elemento (x:xs) = if elemento > x
											then x : (insertarElementoOrdenado elemento xs)
											else elemento : x : xs

-- Propósito: devuelve el elemento más prioriotario (el mínimo) de la priority queue.
-- Precondición: parcial en caso de priority queue vacía.
findMinPQ :: Ord a => PriorityQueue a -> a
findMinPQ (PQ lista) = head lista

-- Propósito: devuelve una priority queue sin el elemento más prioritario (el mínimo).
-- Precondición: parcial en caso de priority queue vacía.
deleteMinPQ :: Ord a => PriorityQueue a -> PriorityQueue a
deleteMinPQ (PQ lista) = PQ (tail lista)