module Set2 (
	Set,
	emptyS,
	addS
	belongs,
	sizeS,
	removeS,
	unionS,
	setToList
)where 

data (Eq a) => Set a = S [a] Int deriving Show

-- Crea un conjunto vacío.
emptyS :: Set a
emptyS = S [] 0

-- Dados un elemento y un conjunto, agrega el elemento al conjunto.
addS :: Eq a => a -> Set a -> Set a
addS e (S lista cant) = if elemen e lista
							then (S lista cant)
							else (S (lista ++ [e]) (length (lista ++ [e]))) 

-- Dados un elemento y un conjunto indica si el elemento pertenece al conjunto.
belongs :: Eq a => a -> Set a -> Bool
belongs e (S lista cant) = elemen e lista

-- Devuelve la cantidad de elementos distintos de un conjunto.
sizeS :: Eq a => Set a -> Int
sizeS (S lista cant) = cant

-- Borra un elemento del conjunto.
removeS :: Eq a => a -> Set a -> Set a
removeS a (S lista cant) = S (eliminarElementoDeLista a lista) (if element a lista then cant - 1 else cant)

eliminarElementoDeLista :: Eq a => a -> [a] -> [a]
eliminarElementoDeLista a [] = []
eliminarElementoDeLista a (x:xs) = if a == x 
									then (eliminarElementoDeLista xs)
									else x : (eliminarElementoDeLista xs)

-- Dados dos conjuntos devuelve un conjunto con todos los elementos de ambos. conjuntos.
unionS :: Eq a => Set a -> Set a -> Set a
unionS (S lista1 c1) (S lista2 c2) = S (sinRepetidos(lista1 ++ lista2)) length(sinRepetidos(lista1 ++ lista2))

sinRepetidos :: Eq a => [a] -> [a]
sinRepetidos [] = []
sinRepetidos (x:xs) = if elemen x xs
						then (sinRepetidos xs)
						else x : (sinRepetidos xs)

-- Dado un conjunto devuelve una lista con todos los elementos distintos del conjunto.
setToList :: Eq a => Set a -> [a]
setToList a (S lista cant) = lista