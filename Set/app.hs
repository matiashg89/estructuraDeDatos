import Set2

ejemploConjunto :: Set int
ejemploConjunto = emptyS

-- Dados una lista y un conjunto, devuelve una lista con todos los elementos que pertenecen
-- al conjunto.
losQuePertenecen :: Eq a => [a] -> Set a -> [a]
losQuePertenecen [] _ = []
losQuePertenecen (x:xs) c = if belongs x c
								then x : (losQuePertenecen xs c)
								else (losQuePertenecen xs c)

-- Quita todos los elementos repetidos de la lista dada utilizando un conjunto como es-
-- tructura auxiliar.
-- sinRepetidos :: Eq a => [a] -> [a]
-- sinRepetidos [] = []
-- sinRepetidos lista = setToList (agregarElementosASet lista emptyS)

-- agregarElementosASet :: Eq a => [a] -> Set a Int
-- agregarElementosASet [] = emptyS
-- agregarElementosASet (x:xs) = 
	
-- Dado un arbol de conjuntos devuelve un conjunto con la union de todos los conjuntos
-- del arbol.
unirTodos :: Eq a => Tree (Set a) -> Set a