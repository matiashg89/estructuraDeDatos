module Set1 (
	Set,
	emptyS,
	addS
	belongs,
	sizeS,
	removeS,
	unionS,
	setToList
)where 

import Set1

data (Eq a) => Set a = S [a] deriving Show

-- Crea un conjunto vacío.
emptyS :: Set a
emptyS = S []

-- Dados un elemento y un conjunto, agrega el elemento al conjunto.
addS :: Eq a => a -> Set a -> Set a
addS e (S lista) = S (lista ++ [e]) 

-- Dados un elemento y un conjunto indica si el elemento pertenece al conjunto.
belongs :: Eq a => a -> Set a -> Bool
belongs e (S lista) = elemen e lista

-- Devuelve la cantidad de elementos distintos de un conjunto.
sizeS :: Eq a => Set a -> Int
sizeS (S lista) = elementosDistintos lista

elementosDistintos :: Eq a => [a] -> Int
elementosDistintos (x:xs) = if element x xs 
							then elementosDistintos xs
							else 1 + (elementosDistintos xs)

-- Borra un elemento del conjunto.
removeS :: Eq a => a -> Set a -> Set a
removeS a (S lista) = S (eliminarElementoDeLista a lista)

eliminarElementoDeLista :: Eq a => a -> [a] -> [a]
eliminarElementoDeLista a [] = []
eliminarElementoDeLista a (x:xs) = if a == x 
									then (eliminarElementoDeLista xs)
									else x : (eliminarElementoDeLista xs)

-- Dados dos conjuntos devuelve un conjunto con todos los elementos de ambos. conjuntos.
unionS :: Eq a => Set a -> Set a -> Set a
unionS (S lista1) (S lista2) = S (lista1 ++ lista2)

-- Dado un conjunto devuelve una lista con todos los elementos distintos del conjunto.
setToList :: Eq a => Set a -> [a]
setToList a (S lista) = (sinRepetidos lista)

sinRepetidos :: Eq a => [a] -> [a]
sinRepetidos [] = []
sinRepetidos (x:xs) = if elemen x xs
						then (sinRepetidos xs)
						else x : (sinRepetidos xs)