module Empresa(
	Empresa,
	consEmpresa,
	buscarPorCUIL,
	empleadosDelSector,
	todosLosCUIL,
	todosLosSectores,
	agregarEmpleado,
	agregarASector,
	borrarEmpleado
) where

import Empleado
import Set1
import Map1

type SectorId = Int
type CUIL = Int

data Empresa = ConsE (Map SectorId (Set Empleado)) (Map CUIL Empleado) deriving Show

-- Propósito: construye una empresa vacía.
-- Costo: O(1)
consEmpresa :: Empresa
consEmpresa = (ConsE emptyM emptyM)

-- Propósito: devuelve el empleado con dicho CUIL.
-- Costo: O(log N)
buscarPorCUIL :: CUIL -> Empresa -> Empleado
buscarPorCUIL cuil (ConsE sectores empleados) = buscarEmpleadoPorCuil cuild empleados

buscarEmpleadoPorCuil :: CUIL -> Map CUIL Empleado -> Empleado 
buscarEmpleadoPorCuil cuil mapEmpleados = lookupM cuil mapEmpleados

-- Propósito: indica los empleados que trabajan en un sector dado.
-- Costo: O(N)
empleadosDelSector :: SectorId -> Empresa -> [Empleado]
empleadosDelSector = error "No implementado"

-- Propósito: indica todos los CUIL de empleados de la empresa.
-- Costo: O(N)
todosLosCUIL :: Empresa -> [CUIL]
todosLosCUIL (ConsE mapSector mapEmpleados) = keys mapEmpleados

-- Propósito: indica todos los sectores de la empresa.
-- Costo: O(N)
todosLosSectores :: Empresa -> [SectorId]
todosLosSectores = error "No implementado"

-- Propósito: agrega un empleado a la empresa, en el que trabajará en dichos sectores y tendrá
-- el CUIL dado.
-- Costo: calcular.
agregarEmpleado :: [SectorId] -> CUIL -> Empresa -> Empresa
agregarEmpleado = error "No implementado"

-- Propósito: agrega un sector al empleado con dicho CUIL.
-- Costo: calcular.
agregarASector :: SectorId -> CUIL -> Empresa -> Empresa
agregarASector = error "No implementado"

-- Propósito: elimina al empleado que posee dicho CUIL.
-- Costo: calcular.
borrarEmpleado :: CUIL -> Empresa -> Empresa
borrarEmpleado = error "No implementado"