-- N * log N
agregarASector :: SectorId -> CUIL -> Empresa -> Empresa
agregarASector s c (ConsE ms mc) =
    ConsE (actualizarEmpleadoMS c s ms mc)
          (actualizarEmpleadoMC c s mc)

-- N * log N
actualizarEmpleadoMS :: SectorId
                             -> CUIL
                             -> Map SectorId (Set Empleado)
                             -> Map CUIL Empleado
                             -> Map SectorId (Set Empleado)
actualizarEmpleadoMS s c ms mc =
    actualizarSectores
      (sectores (empleadoActualizado c s mc))
      (empleadoActualizado c s mc)
      ms

-- N * log N
actualizarSectores [] e ms = ms
actualizarSectores (s:sectores) e ms =
    actualizarSector s e (actualizarSectores sectores e ms)

-- log N
actualizarSector :: SectorId -> Empleado
                             -> Map SectorId (Set Empleado)
                             -> Map SectorId (Set Empleado)
actualizarSector s e m =
    case lookupM s m of
        Nothing -> assocM s (addS e emptyS) m
        Just empleados -> assocM s (actualizarSetEmpleados e empleados) m

-- log N
actualizarSetEmpleados e s = addS e (removeS e s)

-- log N
actualizarEmpleadoMC :: CUIL -> SectorId -> Map CUIL Empleado -> Map CUIL Empleado
actualizarEmpleadoMC c s m = assocM c (empleadoActualizado c s m) m

-- log N
empleadoActualizado :: CUIL -> SectorId -> Map CUIL Empleado -> Empleado
empleadoActualizado c s m =
    case lookupM c m of
        Nothing -> agregarSector s (consEmpleado c)
        Just e -> agregarSector s e
