module Map1(
	Map,emptyM,assocM,lookupM,deleteM,keys
) where

data Map k v = M [k][v] deriving Show


-- Propósito: devuelve un map vacío
emptyM :: Map k v
emptyM = M [][]

-- Propósito: agrega una asociación clave-valor al map.
assocM :: Eq k => k -> v -> Map k v -> Map k v
assocM key val (M k v) = if not (existeKey key k)
							then M  (key : k) (val : v )
							else M k v

existeKey :: Eq k => k -> [k] -> Bool
existeKey _ [] = False
existeKey k (x:xs) = (k == x) || (existeKey k xs)

-- Propósito: encuentra un valor dado una clave.
lookupM :: Eq k => k -> Map k v -> Maybe v
lookupM key (M k v) = buscarClaveYRetornarValor key k v

buscarClaveYRetornarValor :: Eq k => k -> [k] -> [v] -> Maybe v 
buscarClaveYRetornarValor _ [] _ = Nothing
buscarClaveYRetornarValor _ _ [] = Nothing
buscarClaveYRetornarValor key (k:ks) (v:vs) = if k == key
											   then Just v
											   else buscarClaveYRetornarValor key ks vs

-- Propósito: borra una asociación dada una clave.
deleteM :: Eq k => k -> Map k v -> Map k v
deleteM key (M k v) = buscarKeyYBorrar key k v

buscarKeyYBorrar :: Eq k => k -> [k] -> [v] -> Map k v
buscarKeyYBorrar _ [] [] = error "Clave no encontrada"
buscarKeyYBorrar key (k:ks) (v:vs) = if key == k
										then M ks vs
										else buscarKeyYBorrar key ks vs

-- Propósito: devuelve las claves del map.
keys :: Map k v -> [k]
keys (M k v) = k