module Empleado(
	Empleado,
	consEmpleado,
	cuil,
	agregarSector,
	sectores
) where

import Set1

type SectorId = Int  
type CUIL = Int

data Empleado = Empleado CUIL (Set SectorId) deriving Show

-- Propósito: construye un empleado con dicho CUIL.
-- Costo: O(1)
consEmpleado :: CUIL -> Empleado
consEmpleado c = (Empleado c emptyS)


-- Propósito: indica el CUIL de un empleado.
-- Costo: O(1)
cuil :: Empleado -> CUIL
cuil (Empleado cuil _) = cuil


-- Propósito: agrega un sector a un empleado.
-- Costo: O(log N)
agregarSector :: SectorId -> Empleado -> Empleado
agregarSector s (Empleado c sectores) = (Empleado c (addS s sectores))

-- Propósito: indica los sectores en los que el empleado trabaja.
-- Costo: O(N)
sectores :: Empleado -> (Set SectorId)
sectores (Empleado c sec) = sec