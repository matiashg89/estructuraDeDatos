import Map1

module MultiSet1(
	MultiSet,emptyM,addMS,ocurrencesMS, unionMS, intersectionMS, multiSetToList
) where

data MultiSet k v = MS (Map k v) deriving Show

-- Propósito: denota un multiconjunto vacío.
emptyMS :: MultiSet a

-- Propósito: dados un elemento y un multiconjunto, agrega una ocurrencia de ese elemento al
-- multiconjunto.
addMS :: Ord a => a -> MultiSet a -> MultiSet a

-- Propósito: dados un elemento y un multiconjunto indica la cantidad de apariciones de ese
-- elemento en el multiconjunto.
ocurrencesMS :: Ord a => a -> MultiSet a -> Int

-- Propósito: dados dos multiconjuntos devuelve un multiconjunto con todos los elementos de
-- ambos multiconjuntos. (opcional)
unionMS :: Ord a => MultiSet a -> MultiSet a -> MultiSet a 

-- Propósito: dados dos multiconjuntos devuelve el multiconjunto de elementos que ambos
-- multiconjuntos tienen en común. (opcional)
intersectionMS :: Ord a => MultiSet a -> MultiSet a -> MultiSet a 

-- Propósito: dado un multiconjunto devuelve una lista con todos los elementos del conjunto y
-- su cantidad de ocurrencias.
multiSetToList :: MultiSet a -> [(a, Int)]