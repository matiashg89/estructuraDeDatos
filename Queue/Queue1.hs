module Queue1(
	Queue,
	emptyQ,
	isEmptyQ,
	queue,
	firstQ,
	dequeue
) where

data (Eq a) => Queue a = Q [a] deriving Show

-- Crea una cola vacía.
emptyQ :: (Eq a) => Queue a
emptyQ = Q []

-- Dada una cola indica si la cola está vacía.
isEmptyQ :: (Eq a) => Queue a -> Bool
isEmptyQ (Q l) = null l

-- Dados un elemento y una cola, agrega ese elemento a la cola.
queue :: (Eq a) =>  a -> Queue a -> Queue a
queue n (Q l) = Q (l++[n])

-- Dada una cola devuelve el primer elemento de la cola.
firstQ :: (Eq a) => Queue a -> a
firstQ (Q l) = if null l then error "La lista esta vacia" else head l

-- Dada una cola la devuelve sin su primer elemento.
dequeue :: (Eq a) => Queue a -> Queue a
dequeue (Q l) = if null l then error "La lista esta vacia" else Q (tail l)