import Queue1

ejemploCola :: (Eq a) => Queue a
ejemploCola = emptyQ

-- Cuenta la cantidad de elementos de la cola.
lengthQ :: (Eq a) => Queue a -> Int
lengthQ cola = if (isEmptyQ cola) 
				then 0
				else 1 + (lengthQ (dequeue cola))

-- Dada una cola devuelve la lista con los mismos elementos,
-- donde el orden de la lista es el de la cola.
-- Nota: chequear que los elementos queden en el orden correcto.
queueToList :: (Eq a) => Queue a -> [a]
queueToList cola = [firstQ cola] ++ (queueToList cola)

-- Inserta todos los elementos de la segunda cola en la primera.
unionQ :: Queue a -> Queue a -> Queue a
unionQ cola1 cola2 = unionQ (queue (firstQ cola1) cola1 ) (dequeue cola2)